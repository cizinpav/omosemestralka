import cvut.fel.tauchda.cizinpav.Configuration.Configurator;
import cvut.fel.tauchda.cizinpav.Controller.FridgeController;
import cvut.fel.tauchda.cizinpav.Controller.ManualController;
import cvut.fel.tauchda.cizinpav.Device.Fridge;
import cvut.fel.tauchda.cizinpav.DeviceCommands.DeviceCommand;
import cvut.fel.tauchda.cizinpav.DeviceCommands.GeneralCommands.TurnOffCommand;
import cvut.fel.tauchda.cizinpav.Entities.Floor;
import cvut.fel.tauchda.cizinpav.Entities.House;
import cvut.fel.tauchda.cizinpav.Entities.Room;
import cvut.fel.tauchda.cizinpav.Sensor.FridgeSensor;
import cvut.fel.tauchda.cizinpav.Sensor.Sensor;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

@DisplayName("Configuration load and save test")
public class ConfigureTest {
    House house;
    Floor floor;
    Room room;
    Fridge fridge;
    Sensor sensor;
    ManualController fridgeController;
    DeviceCommand fridgeTurnOffCommand;
    Configurator configurator;

    @BeforeEach
    public void prepare(){
        house = new House(0, "house", "my street", "my town");
        floor = new Floor(0);
        room = new Room(0, "my room");

        floor.addRoom(room);
        house.addFloor(floor);

        fridge = new Fridge(0, "my fridge", room);
        sensor = new FridgeSensor(0, room, fridge);
        fridgeController = new FridgeController(fridge);

        fridgeTurnOffCommand = new TurnOffCommand(fridge);
        fridgeController = fridge.getController();

        fridgeController.addCommand(fridgeTurnOffCommand);

        configurator = new Configurator();
    }

    @Test
    public void saveConfiguration(){
        List<House> houses = new ArrayList<>();
        houses.add(house);
        configurator.saveConfiguration(houses);
    }

    @Test
    public void loadConfiguration(){
        List<House> houses = configurator.loadConfiguration();
        System.out.println(houses);
    }

}
