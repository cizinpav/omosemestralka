import cvut.fel.tauchda.cizinpav.Configuration.Configurator;
import cvut.fel.tauchda.cizinpav.Controller.AutomatedController;
import cvut.fel.tauchda.cizinpav.Device.*;
import cvut.fel.tauchda.cizinpav.DeviceCommands.DeviceCommand;
import cvut.fel.tauchda.cizinpav.DeviceCommands.GeneralCommands.TurnOffCommand;
import cvut.fel.tauchda.cizinpav.DeviceCommands.TvCommands.TvQuiteCommand;
import cvut.fel.tauchda.cizinpav.Entities.*;
import cvut.fel.tauchda.cizinpav.Instruction.ExecutionConstraints.ExecutionConstrainFactory;
import cvut.fel.tauchda.cizinpav.Instruction.ExecutionConstraints.ExecutionConstraint;
import cvut.fel.tauchda.cizinpav.Instruction.Instruction;
import cvut.fel.tauchda.cizinpav.Instruction.InstructionFactory;
import cvut.fel.tauchda.cizinpav.Logger.AllSensorsLogger;
import cvut.fel.tauchda.cizinpav.Logger.CacheLogger;
import cvut.fel.tauchda.cizinpav.Logger.ConsumptionLogger;
import cvut.fel.tauchda.cizinpav.Sensor.*;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class ConfigurationSetupUtil {
    @Test
    public void configurationBuilder(){
        // Initializing variables
        List<House> houses = new ArrayList<>();
        List<Floor> floors = new ArrayList<>();
        List<Room> rooms = new ArrayList<>();
        List<Transport> transports = new ArrayList<>();
        List<Pet> pets = new ArrayList<>();
        List<Occupant> occupants = new ArrayList<>();
        List<Device> devices = new ArrayList<>();
        List<Sensor> sensors = new ArrayList<>();

        // Used for saving a retrieving current sensor data
        SensorDataCache cache = new SensorDataCache();
        // Subscriber that passes data to cache
        CacheLogger cacheLogger = new CacheLogger(cache);

        // Logger that cache data from ConsumptionSensor and writes them to a json
        // Default path is ./consumptionReport.json
        // To change path pass it as a string to a constructor (without the file extension)
        ConsumptionLogger consumptionLogger = new ConsumptionLogger();
        // Used for printing all sensor data to standard output
        AllSensorsLogger allSensorsLogger = new AllSensorsLogger();

        SensorUpdateClock updateClock;

        InstructionFactory instructionFactory = new InstructionFactory(cache);
        ExecutionConstrainFactory constrainFactory = new ExecutionConstrainFactory();

        AutomatedController controller;

        // Making occupants
        // Occupant(idOccupant, occupantName)
        occupants.add(new Occupant(0, "Adam"));
        occupants.add(new Occupant(1, "Petr"));
        occupants.add(new Occupant(2, "Daniela"));

        // Making pets
        // Pet(idPet, petName)
        pets.add(new Pet(0, "Alik"));
        pets.add(new Pet(1, "Micka"));


        // Making Transport
        transports.add(new Car(0, "350 000CZK", "Skoda", "Sedan"));
        transports.add(new Bicycle(1, "Mountain", "MARIN"));


        // Making house
        // House(idHouse, houseType, street, city/town)
        houses.add(new House(0, "studio", "Evropská", "Prague"));


        // Making floors
        // Floor(idFloor)
        floors.add(new Floor(0));
        floors.add(new Floor(1));
        floors.add(new Floor(2));


        // Making rooms
        // Room(idRoom, roomName)
        rooms.add(new Room(0, "Sklep"));

        rooms.add(new Room(1, "Predsin"));
        rooms.add(new Room(2, "Obyvak"));
        rooms.add(new Room(3, "Pracovna"));
        rooms.add(new Room(4, "Kuchyne"));
        rooms.add(new Room(5, "Koupelna"));

        rooms.add(new Room(6, "Detsky pokoj"));
        rooms.add(new Room(7, "Balkon"));
        rooms.add(new Room(8, "Koupelna"));


        // Making devices and adding them to a room
        devices.add(new Tv(0, "Televize", rooms.get(2)));
        devices.add(new CoffeeMachine(1, "Pracovni kavovar", rooms.get(3)));
        devices.add(new CoffeeMachine(2, "Kavovar", rooms.get(4)));
        devices.add(new Fridge(3, "Lednice", rooms.get(4)));

        devices.get(3).getController().addCommand("TurnOff", new TurnOffCommand(devices.get(3)));
        devices.get(0).getController().addCommand("TurnOff", new TurnOffCommand(devices.get(0)));
        devices.get(0).getController().addCommand("TvQuite", new TvQuiteCommand((Tv) devices.get(0)));


        // Making sensors, adding them to a room and for some of them setting devices
        sensors.add(new TimeSensor(0, rooms.get(0)));
        sensors.add(new WindSensor(1, rooms.get(7)));
        sensors.add(new FridgeSensor(2, rooms.get(4), (Fridge) devices.get(3)));
        sensors.add(new ConsumptionSensor(4, rooms.get(0), devices.get(1)));
        sensors.add(new ConsumptionSensor(4, rooms.get(0), devices.get(2)));
        sensors.add(new ConsumptionSensor(3, rooms.get(0), devices.get(3)));

        // Making update clock
        // When run this clock updates sensors
        // SensorUpdateClock(List<Sensor> sensors)
        updateClock = new SensorUpdateClock(sensors);


        // Setting subscriber for sensors
        for (Sensor sensor:
             sensors) {
            // AllSensorsLogger when updated by data from sensor writes this data to standard output
            sensor.addSubscriber(allSensorsLogger);
        }


        // Making instruction
        //// Setting type for ExecutionConstrainFactory so when a new constraint is created it will check only sensors with a specific type
        constrainFactory.setSensorType("Time sensor");
        //// Getting constraint from factory
        ExecutionConstraint timeConstraint = constrainFactory.getLessThanConstrain("Hour", 4);
        //// Getting turnOffCommand from devices controller
        DeviceCommand command = devices.get(3).getController().getCommands().get("TurnOff");
        //// Getting empty instruction with set sensor cache (used cache is declared when initializing instruction factory)
        Instruction instruction = instructionFactory.getNewInstruction();
        //// Setting instructions name
        instruction.setName("Turn off fridge between midnight and 4am");
        //// Setting instruction constraint (if there are more than one constraint all must be satisfied)
        instruction.addConstraint(timeConstraint);
        //// Setting commands to execute when constraint/s are satisfied
        instruction.addCommand(command);


        // Making second instruction
        constrainFactory.setIdSensor(1);
        ExecutionConstraint windConstraint = constrainFactory.getGreaterThanConstrain("wind speed", 120);
        DeviceCommand tvCommand = devices.get(3).getController().getCommands().get("TurnOff");
        Instruction instruction1 = instructionFactory.getNewInstruction();
        instruction1.setName("Turn off Tv if there is hurricane outside");
        instruction1.addConstraint(windConstraint);
        instruction1.addCommand(tvCommand);


        // Declaring controller
        controller = new AutomatedController();
        // Adding instructions
        controller.addInstruction(instruction);
        controller.addInstruction(instruction1);

        // Setting responsibility chain
        allSensorsLogger.setNext(consumptionLogger);
        consumptionLogger.setNext(cacheLogger);
        cacheLogger.setNext(controller);

        // Adding rooms to floors
        floors.get(0).addRoom(rooms.get(0));

        for (int i = 1; i < 6; i++) {
            floors.get(1).addRoom(rooms.get(i));
        }

        floors.get(2).addRoom(rooms.get(6));
        floors.get(2).addRoom(rooms.get(7));
        floors.get(2).addRoom(rooms.get(8));


        // Adding floors to house
        houses.get(0).addFloor(floors.get(0));
        houses.get(0).addFloor(floors.get(1));
        houses.get(0).addFloor(floors.get(2));

        // Setting house SensorUpdateClock
        houses.get(0).setClock(updateClock);
        // Setting house AutomatedController
        houses.get(0).setController(controller);


        // Configurator which will save this configuration
        // Default path of configuration is ./configuration.ser
        // To change path give constructor path (without the file extension)
        Configurator configurator = new Configurator();
        // Save configuration
        // configurator.saveConfiguration(List<House> houses)
        configurator.saveConfiguration(houses);
    }
}
