import cvut.fel.tauchda.cizinpav.Controller.ManualController;
import cvut.fel.tauchda.cizinpav.Device.Fridge;
import cvut.fel.tauchda.cizinpav.DeviceCommands.DeviceCommand;
import cvut.fel.tauchda.cizinpav.DeviceCommands.FridgeCommands.FridgeEmptyCoolingCommand;
import cvut.fel.tauchda.cizinpav.DeviceCommands.FridgeCommands.FridgeMaxCoolingCommand;
import cvut.fel.tauchda.cizinpav.DeviceCommands.GeneralCommands.TurnOffCommand;
import cvut.fel.tauchda.cizinpav.Entities.Room;
import cvut.fel.tauchda.cizinpav.Sensor.FridgeSensor;
import cvut.fel.tauchda.cizinpav.Sensor.Sensor;
import cvut.fel.tauchda.cizinpav.Sensor.SensorData;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


@DisplayName("Simple Device and Sensor tests")
public class SimpleTest {
    Room room;
    SensorData data;
    Fridge fridge;
    Sensor fridgeSensor;
    ManualController fridgeController;
    DeviceCommand turnOffCommand;
    DeviceCommand emptyCoolingLevel;
    DeviceCommand maxCoolingLevel;


    @BeforeEach
    public void prepare() {
        room = new Room(0, "Muj pokoj");
        fridge = new Fridge(0, "Moje lednice", room);
        fridgeController = fridge.getController();
        fridgeSensor = new FridgeSensor(0, fridge.getRoom(), fridge);
        turnOffCommand = new TurnOffCommand(fridge);
        emptyCoolingLevel = new FridgeEmptyCoolingCommand(fridge);
        maxCoolingLevel = new FridgeMaxCoolingCommand(fridge);
    }

    @Test
    @DisplayName("Constructor test")
    // Testing fridge constructor
    public void fridgeConstruction() {
        Assertions.assertEquals(fridge.getName(), "Moje lednice");
        Assertions.assertEquals(3, fridge.getCoolingLevel());
        Assertions.assertTrue(fridge.getContains().isEmpty());
    }

    @Test
    @DisplayName("Fridge sensor test")
    // Testing if sensor reflexes fridge real state
    public void fridgeSensorTest() {
        fridgeSensor.update();
        data = new SensorData(fridgeSensor.getIdSensor(), "Fridge sensor", fridgeSensor.getRoom(), fridgeSensor.getData());

        assertEquals(1, data.getData().get("fridge empty"));

        fridge.addContains("banana", 5);
        fridgeSensor.update();
        data = new SensorData(fridgeSensor.getIdSensor(), "Fridge sensor", fridgeSensor.getRoom(), fridgeSensor.getData());

        assertEquals(0, data.getData().get("fridge empty"));
        assertEquals(5, data.getData().get("food quantity"));
    }
}