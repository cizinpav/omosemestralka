import cvut.fel.tauchda.cizinpav.Logger.CacheLogger;
import cvut.fel.tauchda.cizinpav.Entities.Room;
import cvut.fel.tauchda.cizinpav.Sensor.Sensor;
import cvut.fel.tauchda.cizinpav.Sensor.SensorData;
import cvut.fel.tauchda.cizinpav.Sensor.SensorDataCache;
import cvut.fel.tauchda.cizinpav.Sensor.WindSensor;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

@DisplayName("Simple cache tests")
public class CacheTest {
    private SensorDataCache cache;
    private CacheLogger logger;
    private Room room;
    private Sensor sensor;

    @BeforeEach
    public void prepare() {
        cache = new SensorDataCache();
        logger = new CacheLogger(cache);

        room = new Room(0, "my room");
        sensor = new WindSensor(0, room);

        sensor.addSubscriber(logger);
    }

    @Test
    @DisplayName("Simple test")
    public void simpleTest() {
        sensor.update();

        Assertions.assertNotEquals(0, cache.getDataById(0).getData().size());
        Assertions.assertEquals(logger.getData(), cache.getDataById(0));
    }

    @Test
    @DisplayName("Value from same sensor changes value in cache")
    public void advancedTest() {
        SensorData oldData;

        sensor.update();
        oldData = logger.getData();
        sensor.update();

        Assertions.assertNotEquals(0, cache.getDataById(0).getData().size());
        Assertions.assertNotEquals(oldData, cache.getDataById(0));

        Assertions.assertEquals(logger.getData(), cache.getDataById(0));
        Assertions.assertEquals(cache.getDataById(0), cache.getDataBySensorType("Wind sensor").get(0));
    }
}
