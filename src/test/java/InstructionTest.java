import cvut.fel.tauchda.cizinpav.Configuration.Configurator;
import cvut.fel.tauchda.cizinpav.Controller.ManualController;
import cvut.fel.tauchda.cizinpav.Device.Fridge;
import cvut.fel.tauchda.cizinpav.DeviceCommands.DeviceCommand;
import cvut.fel.tauchda.cizinpav.DeviceCommands.FridgeCommands.FridgeBreezeCoolingCommand;
import cvut.fel.tauchda.cizinpav.DeviceCommands.FridgeCommands.FridgeEmptyCoolingCommand;
import cvut.fel.tauchda.cizinpav.DeviceCommands.FridgeCommands.FridgeMaxCoolingCommand;
import cvut.fel.tauchda.cizinpav.DeviceCommands.GeneralCommands.TurnOffCommand;
import cvut.fel.tauchda.cizinpav.Entities.Floor;
import cvut.fel.tauchda.cizinpav.Entities.House;
import cvut.fel.tauchda.cizinpav.Instruction.ExecutionConstraints.ExecutionConstrainFactory;
import cvut.fel.tauchda.cizinpav.Instruction.ExecutionConstraints.ExecutionConstraint;
import cvut.fel.tauchda.cizinpav.Instruction.Instruction;
import cvut.fel.tauchda.cizinpav.Logger.CacheLogger;
import cvut.fel.tauchda.cizinpav.Entities.Room;
import cvut.fel.tauchda.cizinpav.Sensor.FridgeSensor;
import cvut.fel.tauchda.cizinpav.Sensor.Sensor;
import cvut.fel.tauchda.cizinpav.Sensor.SensorData;
import cvut.fel.tauchda.cizinpav.Sensor.SensorDataCache;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("Simple Instruction and ExecutableConstrains tests")
class InstructionTest {
    House house;
    Floor floor;
    Room room;
    SensorData data;
    Fridge fridge;
    Sensor fridgeSensor;
    ManualController fridgeController;
    DeviceCommand turnOffCommand;
    DeviceCommand emptyCoolingLevel;
    DeviceCommand breezeCoolingLevel;
    DeviceCommand maxCoolingLevel;
    ExecutionConstrainFactory factory;
    SensorDataCache cache;
    CacheLogger logger;

    @BeforeEach
    public void prepare() {
        house = new House(0, "a", "b", "c");
        floor = new Floor(0);
        room = new Room(0, "Muj pokoj");
        floor.addRoom(room);
        house.addFloor(floor);
        fridge = new Fridge(0, "Moje lednice", room);
        fridgeController = fridge.getController();
        fridgeSensor = new FridgeSensor(0, fridge.getRoom(), fridge);
        turnOffCommand = new TurnOffCommand(fridge);
        emptyCoolingLevel = new FridgeEmptyCoolingCommand(fridge);
        maxCoolingLevel = new FridgeMaxCoolingCommand(fridge);
        breezeCoolingLevel = new FridgeBreezeCoolingCommand(fridge);
        factory = new ExecutionConstrainFactory(0);
        cache = new SensorDataCache();
        logger = new CacheLogger(cache);
        fridgeSensor.addSubscriber(logger);
    }

    @Test
    @DisplayName("Change fridge cooling level based on food quantity")
    public void emptyFridgeTest() {
        ExecutionConstraint emptyFridgeConstraint = factory.getEqualsConstrain("fridge empty", 1);
        ExecutionConstraint notEmptyConstraint = factory.getNotEqualsConstrain("fridge empty", 1);
        ExecutionConstraint greaterThanFiveConstraint = factory.getGreaterThanConstrain("food quantity", 5);

        Instruction emptyInstruction = new Instruction(0, "empty fridge", emptyFridgeConstraint, emptyCoolingLevel, cache);
        Instruction notEmptyInstruction = new Instruction(1, "not empty fridge", notEmptyConstraint, breezeCoolingLevel, cache);
        Instruction fullInstruction = new Instruction(2, "full fridge", greaterThanFiveConstraint, maxCoolingLevel, cache);

        fridgeSensor.update();
        data = new SensorData(fridgeSensor.getIdSensor(), "Fridge sensor", fridgeSensor.getRoom(), fridgeSensor.getData());

        assertTrue(emptyInstruction.shouldBeExecuted(data));
        assertFalse(notEmptyInstruction.shouldBeExecuted(data));
        assertFalse(fullInstruction.shouldBeExecuted(data));

        emptyInstruction.execute();
        assertEquals(1, fridge.getCoolingLevel());

        fridge.addContains("banana", 3);
        fridgeSensor.update();
        data = new SensorData(fridgeSensor.getIdSensor(), "Fridge sensor", fridgeSensor.getRoom(), fridgeSensor.getData());

        assertFalse(emptyInstruction.shouldBeExecuted(data));
        assertTrue(notEmptyInstruction.shouldBeExecuted(data));
        assertFalse(fullInstruction.shouldBeExecuted(data));

        notEmptyInstruction.execute();
        assertEquals(3, fridge.getCoolingLevel());

        fridge.addContains("banana", 3);
        fridgeSensor.update();
        data = new SensorData(fridgeSensor.getIdSensor(), "Fridge sensor", fridgeSensor.getRoom(), fridgeSensor.getData());

        assertFalse(emptyInstruction.shouldBeExecuted(data));
        assertTrue(notEmptyInstruction.shouldBeExecuted(data));
        assertTrue(fullInstruction.shouldBeExecuted(data));

        fullInstruction.execute();
        assertEquals(10, fridge.getCoolingLevel());
    }
}