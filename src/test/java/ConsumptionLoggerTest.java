import cvut.fel.tauchda.cizinpav.Configuration.Configurator;
import cvut.fel.tauchda.cizinpav.Device.Fridge;
import cvut.fel.tauchda.cizinpav.Entities.Floor;
import cvut.fel.tauchda.cizinpav.Entities.House;
import cvut.fel.tauchda.cizinpav.Entities.Room;
import cvut.fel.tauchda.cizinpav.Logger.ConsumptionLogger;
import cvut.fel.tauchda.cizinpav.Sensor.ConsumptionSensor;
import cvut.fel.tauchda.cizinpav.Sensor.Sensor;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

@DisplayName("Consumption logger test")
public class ConsumptionLoggerTest {
    private House house;
    private Floor floor;
    private String fileName;
    private ConsumptionSensor sensor;
    private Room room;
    private Fridge fridge;
    private ConsumptionLogger logger;

    @BeforeEach
    public void prepare(){
        house = new House(0, "something", "something else", "last");
        floor = new Floor(0);
        room = new Room(0, "my room");
        floor.addRoom(room);
        house.addFloor(floor);
        fridge = new Fridge(0, "My fridge", room);

        sensor = new ConsumptionSensor(0, room, fridge);

        logger = new ConsumptionLogger();
        sensor.addSubscriber(logger);
    }

    @Test
    @DisplayName("Simple test")
    public void simpleTest(){
        sensor.update();
    }

    @Test
    @DisplayName("10 Devices")
    public void tenFridges(){
        List<Fridge> fridges = new ArrayList<>();
        List<ConsumptionSensor> sensors = new ArrayList<>();
        room.removeSensor(sensor);

        for (int i = 0; i < 10; i++) {
            fridges.add(new Fridge(i, "Some firdge", room));
            if(i%2 == 0){
                fridges.get(i).setStatus("On");
            }
            sensors.add(new ConsumptionSensor(i, room, fridges.get(i)));
            sensors.get(i).addSubscriber(logger);
        }

        for (Sensor sensorr:
             sensors) {
            sensorr.update();
        }


    }
}
