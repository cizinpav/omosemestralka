import cvut.fel.tauchda.cizinpav.Configuration.Configurator;
import cvut.fel.tauchda.cizinpav.Controller.AutomatedController;
import cvut.fel.tauchda.cizinpav.Device.Fridge;
import cvut.fel.tauchda.cizinpav.DeviceCommands.DeviceCommand;
import cvut.fel.tauchda.cizinpav.DeviceCommands.FridgeCommands.FridgeEmptyCoolingCommand;
import cvut.fel.tauchda.cizinpav.Entities.Floor;
import cvut.fel.tauchda.cizinpav.Entities.House;
import cvut.fel.tauchda.cizinpav.Entities.Room;
import cvut.fel.tauchda.cizinpav.Instruction.ExecutionConstraints.ExecutionConstrainFactory;
import cvut.fel.tauchda.cizinpav.Instruction.ExecutionConstraints.ExecutionConstraint;
import cvut.fel.tauchda.cizinpav.Instruction.Instruction;
import cvut.fel.tauchda.cizinpav.Instruction.InstructionFactory;
import cvut.fel.tauchda.cizinpav.Logger.CacheLogger;
import cvut.fel.tauchda.cizinpav.Sensor.FridgeSensor;
import cvut.fel.tauchda.cizinpav.Sensor.Sensor;
import cvut.fel.tauchda.cizinpav.Sensor.SensorDataCache;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

@DisplayName("Activity logger test")
public class ActivityLoggerTest {
    private House house;
    private Floor floor;
    private String fileName;
    private Sensor sensor;
    private SensorDataCache cache;
    private CacheLogger cacheLogger;
    private Room room;
    private AutomatedController controller;
    private ExecutionConstrainFactory constrainFactory;
    private InstructionFactory instructionFactory;
    private Fridge fridge;
    private File file;


    @BeforeEach
    public void prepare(){
        fileName = "activityTestLog";
        controller = new AutomatedController();
//        controller.setActivityLoggerFile(fileName);


        cache = new SensorDataCache();
        cacheLogger = new CacheLogger(cache);
        cacheLogger.setNext(controller);

        constrainFactory = new ExecutionConstrainFactory(0);
        instructionFactory = new InstructionFactory(cache);

        house = new House(0, "a", "b", "c");
        floor = new Floor(0);
        room = new Room(0, "my room");
        floor.addRoom(room);
        house.addFloor(floor);
        house.setController(controller);

        fridge = new Fridge(0, "My fridge", room);

        sensor = new FridgeSensor(0, room, fridge);

        sensor.addSubscriber(cacheLogger);

        file = new File(fileName+".json");
        if(file.exists()){
            file.delete();
        }
    }

    @Test
    @DisplayName("Simple test")
    public void simpleTest() {
        Instruction instruction = instructionFactory.getNewInstruction();
        ExecutionConstraint constraint = constrainFactory.getEqualsConstrain("fridge empty", 1.);
        DeviceCommand command = new FridgeEmptyCoolingCommand(fridge);
        instruction.addConstraint(constraint);
        instruction.addCommand(command);
        controller.addInstruction(instruction);

        sensor.update();

        Assertions.assertTrue(file.exists());
        String time = LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss"));

        Scanner reader = null;
        try {
            reader = new Scanner(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        String fileText = reader.nextLine();

        String expectedText = "{\"AutomatedController\":[{\"Action\":\"FridgeEmptyCooling\",\"IdDevice\":0,\"NameDevice\":\"My fridge\",\"Time\":\""+time+"\",\"CallerType\":\"Instruction\"}]}";

        Assertions.assertEquals(expectedText, fileText);
    }

}
