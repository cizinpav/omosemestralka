import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import cvut.fel.tauchda.cizinpav.Controller.FridgeController;
import cvut.fel.tauchda.cizinpav.Controller.ManualController;
import cvut.fel.tauchda.cizinpav.Device.Fridge;
import cvut.fel.tauchda.cizinpav.DeviceCommands.DeviceCommand;
import cvut.fel.tauchda.cizinpav.DeviceCommands.GeneralCommands.TurnOffCommand;
import cvut.fel.tauchda.cizinpav.Entities.Floor;
import cvut.fel.tauchda.cizinpav.Entities.House;
import cvut.fel.tauchda.cizinpav.Entities.Room;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.*;

public class jacksonTest {
    House house;
    Floor floor;
    Room room;
    Fridge fridge;
    ManualController fridgeController;
    DeviceCommand fridgeTurnOffCommand;

    @BeforeEach
    public void prepare(){
        house = new House(0, "house", "my street", "my town");
        floor = new Floor(0);
        room = new Room(0, "my room");

        floor.addRoom(room);
        house.addFloor(floor);

        fridge = new Fridge(0, "my fridge", room);

        fridgeTurnOffCommand = new TurnOffCommand(fridge);
        fridgeController = fridge.getController();
        fridgeController.addCommand(fridgeTurnOffCommand);
        fridgeController = new FridgeController(fridge);

    }

    @Test
    public void testJackson(){
        ObjectMapper json = new ObjectMapper();

        try {
            System.out.println(json.writeValueAsString(house));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        DeviceCommand house1 = null;
        try {
            house1 = json.readValue(json.writeValueAsString(fridgeTurnOffCommand), TurnOffCommand.class);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        System.out.println(house1);
    }

    @Test
    public void objectSerialization(){
        try {
            FileOutputStream file = new FileOutputStream("test.ser");
            ObjectOutputStream out = new ObjectOutputStream(file);
            out.writeObject(house);

            FileInputStream fileIn = new FileInputStream("test.ser");
            ObjectInputStream in = new ObjectInputStream(fileIn);

            House temp = (House) in.readObject();

            System.out.println(temp);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

}
