package cvut.fel.tauchda.cizinpav.Device;

import cvut.fel.tauchda.cizinpav.Controller.ManualController;
import cvut.fel.tauchda.cizinpav.Entities.Room;

public class CoffeeMachine extends Device{
    public CoffeeMachine(int idDevice, String name, Room room) {
        super(idDevice, name, room);
    }

    public CoffeeMachine(int idDevice, String name, ManualController controller, Room room) {
        super(idDevice, name, controller, room);
    }

    public CoffeeMachine(int idDevice, String name, String status, ManualController controller, Room room) {
        super(idDevice, name, status, controller, room);
    }
}
