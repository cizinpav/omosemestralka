package cvut.fel.tauchda.cizinpav.Device;

import cvut.fel.tauchda.cizinpav.Controller.ManualController;
import cvut.fel.tauchda.cizinpav.Entities.Room;

public class AlarmClock extends Device{
    public int volume = 100;

    public AlarmClock(int idDevice, String name, Room room) {
        super(idDevice, name, room);
    }

    public AlarmClock(int idDevice, String name, ManualController controller, Room room) {
        super(idDevice, name, controller, room);
    }

    public AlarmClock(int idDevice, String name, String status, ManualController controller, Room room) {
        super(idDevice, name, status, controller, room);
    }

    public int getVolume() {
        return volume;
    }

    public void setVolume(int volume) {
        this.volume = volume;
    }
}
