package cvut.fel.tauchda.cizinpav.Device;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import cvut.fel.tauchda.cizinpav.Controller.ManualController;
import cvut.fel.tauchda.cizinpav.Entities.Room;

import java.io.Serializable;

/**
 * Abstract class for all devices
 */
@JsonIgnoreProperties(value = {"room"})
public abstract class Device implements Serializable {
    /**
     * Unique identifier of the {@link Device}.
     */
    private final int idDevice;

    /**
     * Name of the {@link Device} choose by user (don't have to be unique, but it is preferable).
     */
    private String name;

    /**
     * Momentary status of the {@link Device}.
     * Subclasses of {@link Device} have their own statuses (dishwasher->Doing cycle one; cd player->Playing cd) but all of them have 'Off' status.
     */
    private String status;
    /**
     * {@link ManualController} to be associated with {@link Device}.
     */
    private ManualController controller;
    /**
     * {@link Room} where is {@link Device} located.
     */
    private Room room;


    /**
     * Minimal constructor without {@link ManualController}.
     * This constructor should be followed by setting the {@link ManualController}.
     * {@link Device} don`t necessarily need {@link ManualController} to work but without it, they can`t be controlled remotely (or even locally).
     *
     * @param idDevice Unique identifier of the {@link Device}.
     * @param name     Name of the {@link Device} choose by user (don't have to be unique, but it is preferable).
     * @param room     {@link Room} where is {@link Device} located.
     */
    public Device(int idDevice, String name, Room room) {
        this.idDevice = idDevice;
        this.name = name;
        this.status = "Off";
        this.controller = null;
        this.room = room;
        this.room.addDevice(this);
    }

    /**
     * Standard constructor.
     *
     * @param idDevice   Unique identifier of the {@link Device}.
     * @param name       Name of the {@link Device} choose by user (don't have to be unique, but it is preferable).
     * @param controller {@link ManualController} to be associated with {@link Device}.
     * @param room       {@link Room} where is {@link Device} located.
     */
    public Device(int idDevice, String name, ManualController controller, Room room) {
        this.idDevice = idDevice;
        this.name = name;
        this.status = "Off";
        this.controller = controller;
        this.room = room;
        this.room.addDevice(this);
    }

    /**
     * Standard constructor with initialized status.
     *
     * @param idDevice   Unique identifier of the {@link Device}.
     * @param name       Name of the {@link Device} choose by user (don't have to be unique, but it is preferable).
     * @param status     Momentary status of the {@link Device}.
     * @param controller {@link ManualController} to be associated with {@link Device}.
     * @param room       {@link Room} where is {@link Device} located.
     */
    public Device(int idDevice, String name, String status, ManualController controller, Room room) {
        this.idDevice = idDevice;
        this.name = name;
        this.status = status;
        this.controller = controller;
        this.room = room;
        this.room.addDevice(this);
    }

    /**
     * {@link Device} identifier getter
     *
     * @return unique {@link Device} identifier.
     */
    public int getIdDevice() {
        return idDevice;
    }

    /**
     * {@link Device} name getter.
     *
     * @return {@link Device} name.
     */
    public String getName() {
        return name;
    }


    /**
     * {@link Device} name setter.
     *
     * @param name set new name of the {@link Device}.
     */
    public void setName(String name) {
        this.name = name;
    }


    /**
     * {@link Device} status getter.
     *
     * @return momentary status of the {@link Device}.
     */
    public String getStatus() {
        return status;
    }

    /**
     * {@link Device} status setter.
     *
     * @param status new status of the {@link Device}.
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * {@link Device} controller getter.
     *
     * @return {@link ManualController} associated with the {@link Device}.
     */
    public ManualController getController() {
        return controller;
    }

    /**
     * {@link Device} controller setter. (Shouldn't be needed)
     *
     * @param controller {@link ManualController} to be associated with the {@link Device}.
     */
    public void setController(ManualController controller) {
        this.controller = controller;
    }

    /**
     * {@link Device} room getter.
     *
     * @return {@link Room} where the {@link Device} is located.
     */
    public Room getRoom() {
        return room;
    }

    /**
     * {@link Device} room setter.
     *
     * @param room {@link Room} where the {@link Device} is located.
     */
    public void setRoom(Room room) {
        this.room.removeDevice(this);
        this.room = room;
        this.room.addDevice(this);
    }
}
