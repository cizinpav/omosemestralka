package cvut.fel.tauchda.cizinpav.Device;

import cvut.fel.tauchda.cizinpav.Controller.FridgeController;
import cvut.fel.tauchda.cizinpav.Entities.Room;

import java.util.HashMap;
import java.util.Map;

/**
 * Subclass of {@link Device}
 */
public class Fridge extends Device {
    /**
     * HashMap where key represent food and values it quantity stored in the {@link Fridge}.
     */
    private Map<String, Integer> contains = new HashMap<>();
    /**
     * Level of cooling (Higher means cooler temperature).
     */
    private int coolingLevel = 3;

    /**
     * Constructor
     *
     * @param idDevice unique identifier of the {@link Device}
     * @param name     name of the {@link Fridge}.
     * @param room     {@link Room} where the fridge is located.
     * @see Device for more information on constructor.
     */
    public Fridge(int idDevice, String name, Room room) {
        super(idDevice, name, room);
        setController(new FridgeController(this)); // Makes new ManualController, pass itself as parameter and set it as it`s new ManualController.
    }

    /**
     * {@link Fridge} contains getter.
     *
     * @return Map where keys represent food and values its stored quantity.
     */
    public Map<String, Integer> getContains() {
        return contains;
    }

    /**
     * {@link Fridge} contains setter.
     *
     * @param contains Map of foods and their quantities stored in the {@link Fridge}.
     */
    public void setContains(Map<String, Integer> contains) {
        this.contains = contains;
    }

    /**
     * Add quantity of food into the {@link Fridge}.
     *
     * @param food     food to be stored.
     * @param quantity quantity of the food to be stored.
     */
    public void addContains(String food, int quantity) {
        if (this.contains.get(food) != null) { // If key food is already in the map than add quantity to what`s stored in the fridge.
            this.contains.put(food, this.contains.get(food) + quantity);
        } else {  // Else add this food with the quantity as new record.
            this.contains.put(food, quantity);
        }
    }


    /**
     * Remove quantity of food from the {@link Fridge}.
     *
     * @param food     food from which quantity will be subtracted.
     * @param quantity quantity of food to be removed
     */
    public void removeContains(String food, int quantity) {
        int containsQuantity = this.contains.get(food) - quantity;

        if (containsQuantity < 0) { // If new quantity is negative number remove record with food.
            this.contains.remove(food);
        } else {
            this.contains.put(food, containsQuantity); // Else save new quantity
        }
    }


    /**
     * {@link Fridge} coolingLevel getter.
     *
     * @return level of cooling to which the {@link Fridge} is set.
     */
    public int getCoolingLevel() {
        return coolingLevel;
    }


    /**
     * {@link Fridge} coolingLevel setter.
     *
     * @param coolingLevel level of cooling to which set the {@link Fridge}
     */
    public void setCoolingLevel(int coolingLevel) {
        this.coolingLevel = coolingLevel;
    }
}
