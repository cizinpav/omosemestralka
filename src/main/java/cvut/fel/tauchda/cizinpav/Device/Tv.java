package cvut.fel.tauchda.cizinpav.Device;

import cvut.fel.tauchda.cizinpav.Controller.ManualController;
import cvut.fel.tauchda.cizinpav.Controller.TvController;
import cvut.fel.tauchda.cizinpav.Entities.Room;

public class Tv extends Device{
    private int chanel = 0;
    private int volume = 20;

    public Tv(int idDevice, String name, Room room) {
        super(idDevice, name, room);
        setController(new TvController(this));
    }

    public Tv(int idDevice, String name, ManualController controller, Room room) {
        super(idDevice, name, controller, room);
        setController(new TvController(this));
    }

    public Tv(int idDevice, String name, String status, ManualController controller, Room room) {
        super(idDevice, name, status, controller, room);
        setController(new TvController(this));
    }

    public int getChanel() {
        return chanel;
    }

    public void setChanel(int chanel) {
        this.chanel = chanel;
    }

    public int getVolume() {
        return volume;
    }

    public void setVolume(int volume) {
        this.volume = volume;
    }
}
