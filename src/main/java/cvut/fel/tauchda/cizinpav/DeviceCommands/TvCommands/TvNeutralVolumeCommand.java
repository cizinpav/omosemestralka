package cvut.fel.tauchda.cizinpav.DeviceCommands.TvCommands;

import cvut.fel.tauchda.cizinpav.Device.Device;
import cvut.fel.tauchda.cizinpav.Device.Tv;
import cvut.fel.tauchda.cizinpav.DeviceCommands.DeviceCommand;

public class TvNeutralVolumeCommand extends DeviceCommand {
    private final Tv tv;
    private String action = "TvNeutralVolume";

    public TvNeutralVolumeCommand(Tv tv) {
        this.tv = tv;
    }

    @Override
    public Device getDevice() {
        return tv;
    }

    @Override
    public String getAction() {
        return action;
    }

    @Override
    public void setAction(String action) {
        this.action = action;
    }

    @Override
    public void execute() {
        tv.setVolume(30);
    }
}
