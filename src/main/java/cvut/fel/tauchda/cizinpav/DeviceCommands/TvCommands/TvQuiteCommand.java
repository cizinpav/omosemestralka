package cvut.fel.tauchda.cizinpav.DeviceCommands.TvCommands;

import cvut.fel.tauchda.cizinpav.Device.Device;
import cvut.fel.tauchda.cizinpav.Device.Tv;
import cvut.fel.tauchda.cizinpav.DeviceCommands.DeviceCommand;

public class TvQuiteCommand extends DeviceCommand {
    private final Tv tv;
    private String action = "TvQuite";

    public TvQuiteCommand(Tv tv) {
        this.tv = tv;
    }

    @Override
    public Device getDevice() {
        return tv;
    }

    @Override
    public String getAction() {
        return action;
    }

    @Override
    public void setAction(String action) {
        this.action = action;
    }

    @Override
    public void execute() {
        tv.setVolume(5);
    }
}
