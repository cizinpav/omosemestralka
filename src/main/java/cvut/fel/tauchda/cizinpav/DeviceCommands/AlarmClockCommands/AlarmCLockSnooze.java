package cvut.fel.tauchda.cizinpav.DeviceCommands.AlarmClockCommands;

import cvut.fel.tauchda.cizinpav.Device.AlarmClock;
import cvut.fel.tauchda.cizinpav.Device.Device;
import cvut.fel.tauchda.cizinpav.DeviceCommands.DeviceCommand;

public class AlarmCLockSnooze extends DeviceCommand {
    private final AlarmClock alarmClock;
    private String action = "AlarmClockSnooze";

    public AlarmCLockSnooze(AlarmClock alarmClock) {
        this.alarmClock = alarmClock;
    }

    @Override
    public Device getDevice() {
        return alarmClock;
    }

    @Override
    public String getAction() {
        return action;
    }

    @Override
    public void setAction(String action) {
        this.action = action;
    }

    @Override
    public void execute() {
        this.alarmClock.setStatus("Idle");
    }
}
