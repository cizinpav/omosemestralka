package cvut.fel.tauchda.cizinpav.DeviceCommands.AlarmClockCommands;

import cvut.fel.tauchda.cizinpav.Device.AlarmClock;
import cvut.fel.tauchda.cizinpav.Device.Device;
import cvut.fel.tauchda.cizinpav.DeviceCommands.DeviceCommand;

public class AlarmClockRingCommand extends DeviceCommand {
    private final AlarmClock alarmClock;
    private String action = "AlarmClockRing";

    public AlarmClockRingCommand(AlarmClock alarmClock) {
        this.alarmClock = alarmClock;
    }

    public Device getDevice() {
        return alarmClock;
    }

    public String getAction() {
        return action;
    }

    @Override
    public void setAction(String action) {
        this.action = action;
    }

    public void execute() {
        this.alarmClock.setVolume(100);
        this.alarmClock.setStatus("Ringing");
    }
}
