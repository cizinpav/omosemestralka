package cvut.fel.tauchda.cizinpav.DeviceCommands;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import cvut.fel.tauchda.cizinpav.Device.Device;
import cvut.fel.tauchda.cizinpav.DeviceCommands.GeneralCommands.TurnOffCommand;

import java.io.Serializable;

/**
 * Interface for commands that manipulate with device.
 */
@JsonIgnoreProperties(value = "device")
public abstract class DeviceCommand implements Serializable {
    public DeviceCommand() {
    }

    public abstract Device getDevice();

    public abstract String getAction();

    public abstract void setAction(String action);

    /**
     * All implementing classes need to implement function execute.
     */
    public abstract void execute();
}
