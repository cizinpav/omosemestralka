package cvut.fel.tauchda.cizinpav.DeviceCommands.GeneralCommands;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import cvut.fel.tauchda.cizinpav.Device.Device;
import cvut.fel.tauchda.cizinpav.DeviceCommands.DeviceCommand;

/**
 * Generic command that turn {@link Device} off (works for all {@link Device})
 */ 
public class TurnOffCommand extends DeviceCommand {
    private Device device;
    private String action = "TurnOff";

    /**
     * Constructor
     *
     * @param device {@link Device} to be associated with.
     */
    public TurnOffCommand(Device device) {
        this.device = device;
    }


    @Override
    public Device getDevice() {
        return this.device;
    }

    public void setDevice(Device device) {
        this.device = device;
    }

    @Override
    public String getAction() {
        return this.action;

    }

    @Override
    public void setAction(String action) {
        this.action = action;
    }

    /**
     * Set status of associated {@link Device} to "Off".
     */
    public void execute() {
        device.setStatus("Off");
    }

}
