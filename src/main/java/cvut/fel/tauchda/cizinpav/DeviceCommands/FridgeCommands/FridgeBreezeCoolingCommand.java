package cvut.fel.tauchda.cizinpav.DeviceCommands.FridgeCommands;

import cvut.fel.tauchda.cizinpav.Device.Device;
import cvut.fel.tauchda.cizinpav.Device.Fridge;
import cvut.fel.tauchda.cizinpav.DeviceCommands.DeviceCommand;

/**
 * {@link DeviceCommand} that sets {@link Fridge} cooling level to 3.
 */
public class FridgeBreezeCoolingCommand extends DeviceCommand {
    /**
     * {@link Fridge} to be associated with.
     */
    private final Fridge fridge;

    private String action = "FridgeBreezeCooling";

    /**
     * Constructor
     *
     * @param fridge {@link Fridge} to be associated with.
     */
    public FridgeBreezeCoolingCommand(Fridge fridge) {
        this.fridge = fridge;
    }

    @Override
    public Device getDevice() {
        return fridge;
    }

    @Override
    public String getAction() {
        return action;
    }

    @Override
    public void setAction(String action) {
        this.action = action;
    }

    /**
     * Set cooling level of associated {@link Fridge} to be 3.
     */
    @Override
    public void execute() {
        fridge.setCoolingLevel(3);
    }
}
