package cvut.fel.tauchda.cizinpav.DeviceCommands.TvCommands;

import cvut.fel.tauchda.cizinpav.Device.Device;
import cvut.fel.tauchda.cizinpav.Device.Tv;
import cvut.fel.tauchda.cizinpav.DeviceCommands.DeviceCommand;

public class TvLoudCommand extends DeviceCommand {
    private Tv tv;
    private String action = "TvLoud";

    public TvLoudCommand(Tv tv) {
        this.tv = tv;
    }

    @Override
    public Device getDevice() {
        return this.tv;
    }

    public void setDevice(Tv tv) {
        this.tv = tv;
    }

    @Override
    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    @Override
    public void execute() {tv.setVolume(50);}
}
