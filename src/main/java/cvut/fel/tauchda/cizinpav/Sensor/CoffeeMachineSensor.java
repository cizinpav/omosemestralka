package cvut.fel.tauchda.cizinpav.Sensor;

import cvut.fel.tauchda.cizinpav.Device.CoffeeMachine;
import cvut.fel.tauchda.cizinpav.Entities.Room;
import cvut.fel.tauchda.cizinpav.Subscriber;

import java.util.List;

public class CoffeeMachineSensor extends Sensor{
    private final CoffeeMachine coffeeMachine;

    public CoffeeMachineSensor(int idSensor, Room room, CoffeeMachine coffeeMachine) {
        super(idSensor, "CoffeeMachine sensor", room);
        this.coffeeMachine = coffeeMachine;
    }

    public CoffeeMachineSensor(int idSensor, Room room, List<Subscriber> subscribers, CoffeeMachine coffeeMachine) {
        super(idSensor, "CoffeeMachine sensor", room, subscribers);
        this.coffeeMachine = coffeeMachine;
    }

    @Override
    public void update() {
        double off = this.coffeeMachine.getStatus() == "Off" ? 1. : 0.;
        double brewing = 0;
        double idle = 0;

        if(off != 0) {
            brewing = this.coffeeMachine.getStatus() == "Brewing" ? 1. : 0.;
            idle = this.coffeeMachine.getStatus() == "Idle" ? 1. : 0.;
        }
        this.updateData("Off", off);
        this.updateData("Brewing", brewing);
        this.updateData("Idle", idle);

        notifySubscribers();
    }
}
