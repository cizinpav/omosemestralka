package cvut.fel.tauchda.cizinpav.Sensor;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import cvut.fel.tauchda.cizinpav.Entities.Room;
import cvut.fel.tauchda.cizinpav.Subscriber;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Abstract class for all {@link Sensor}s.
 */
@JsonIgnoreProperties(value = "room")
public abstract class Sensor implements Serializable {
    /**
     * Unique identifier of the {@link Sensor}.
     */
    private final int idSensor;
    /**
     * Type of the {@link Sensor} (ex fridge sensor, wind sensor, atd).
     */
    private final String type;
    /**
     * {@link Room} where is {@link Sensor} located.
     */
    private Room room;
    /**
     * Data read by the {@link Sensor}.
     */
    @JsonIgnore
    private Map<String, Double> data;
    /**
     * List of {@link Subscriber}s to notify when {@link Sensor} change state.
     */
    private List<Subscriber> subscribers;

    /**
     * Minimal constructor
     *
     * @param idSensor Unique identifier of the {@link Sensor}.
     * @param type     Type of the {@link Sensor} (ex fridge sensor, wind sensor, atd).
     * @param room     {@link Room} where is {@link Sensor} located.
     */
    public Sensor(int idSensor, String type, Room room) {
        this.idSensor = idSensor;
        this.type = type;
        this.room = room;
        this.data = new HashMap<String, Double>();
        this.subscribers = new ArrayList<>();
        if(this.room != null) {
            this.room.addSensor(this);
        }
    }

    /**
     * Standard constructor
     *
     * @param idSensor    Unique identifier of the {@link Sensor}.
     * @param type        Type of the {@link Sensor} (ex fridge sensor, wind sensor, atd).
     * @param room        {@link Room} where is {@link Sensor} located.
     * @param subscribers List of {@link Subscriber}s to notify when {@link Sensor} change state.
     */
    public Sensor(int idSensor, String type, Room room, List<Subscriber> subscribers) {
        this.idSensor = idSensor;
        this.type = type;
        this.room = room;
        this.data = new HashMap<String, Double>();
        this.subscribers = subscribers;
        this.room.addSensor(this);
    }

    /**
     * Abstract method to read data from environment and save them to data map.
     */
    public abstract void update();

    /**
     * {@link Sensor} subscriber getter.
     *
     * @return List of {@link Sensor} subscribers.
     * @see Subscriber
     */
    public List<Subscriber> getSubscribers() {
        return subscribers;
    }

    /**
     * {@link Sensor} subscriber setter.
     *
     * @param subscribers List of {@link Subscriber}s to be set as {@link Sensor}s` subscribers.
     */
    public void setSubscribers(List<Subscriber> subscribers) {
        this.subscribers = subscribers;
    }

    /**
     * Adds {@link Subscriber} to {@link Sensor}s' list of {@link Subscriber}s.
     *
     * @param subscriber {@link Subscriber} to be notified on {@link Sensor} update.
     */
    public void addSubscriber(Subscriber subscriber) {
        subscribers.add(subscriber);
    }

    /**
     * Remove {@link Subscriber} from list of {@link Sensor}s' {@link Subscriber}s.
     *
     * @param subscriber {@link Subscriber} to be removed.
     */
    public void removeSubscriber(Subscriber subscriber) {
        subscribers.remove(subscriber);
    }

    /**
     * Update all {@link Subscriber}s in {@link Sensor}s' list of {@link Subscriber}s.
     *
     * @see SensorData
     */
    public void notifySubscribers() {
        SensorData sensorData = new SensorData(idSensor, type, room, data); // Parse into sensor data
        for (Subscriber subscriber :
                subscribers) {
            subscriber.update(sensorData); // updates all subscribers with parsed sensor data
        }
    }

    /**
     * Update {@link Sensor}s` data map with value at key.
     *
     * @param key   key for {@link Sensor}s` data map (if key not in map than it's added. Else update existing value).
     * @param value value to be associated with the key at {@link Sensor}s` data map.
     */
    public void updateData(String key, Double value) {
        data.put(key, value);
    }

    /**
     * Remove record from {@link Sensor}s` data map
     *
     * @param key record key to be removed.
     */
    public void removeData(String key) {
        data.remove(key);
    }

    /**
     * Clears {@link Sensor}s` data map.
     */
    public void purgeData() {
        data.clear();
    }

    /**
     * {@link Sensor} id getter.
     *
     * @return Unique identifier of the {@link Sensor}.
     */
    public int getIdSensor() {
        return idSensor;
    }

    /**
     * {@link Sensor} type getter.
     *
     * @return {@link Sensor} type.
     */
    public String getType() {
        return type;
    }

    /**
     * {@link Sensor} room getter
     *
     * @return {@link Room} where is the {@link Sensor} located.
     */
    public Room getRoom() {
        return room;
    }

    /**
     * {@link Sensor} room setter.
     *
     * @param room {@link Room} where should be the {@link Sensor} located.
     */
    public void setRoom(Room room) {
        this.room.removeSensor(this);
        this.room = room;
        this.room.addSensor(this);
    }

    /**
     * {@link Sensor} data getter.
     *
     * @return Map of {@link Sensor}s` data.
     */
    public Map<String, Double> getData() {
        return data;
    }

    /**
     * {@link Sensor} data setter.
     *
     * @param data Map to be set as {@link Sensor}s` data.
     */
    public void setData(Map<String, Double> data) {
        this.data = data;
    }

}
