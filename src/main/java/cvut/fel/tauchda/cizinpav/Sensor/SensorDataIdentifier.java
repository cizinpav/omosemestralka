package cvut.fel.tauchda.cizinpav.Sensor;

import com.fasterxml.jackson.annotation.JsonIgnore;
import cvut.fel.tauchda.cizinpav.Entities.Room;

import java.io.Serializable;

/**
 * Record used as key in {@link SensorDataCache} to enable searching {@link SensorData} by id, sensorType or by sensorType and room.
 */
public record SensorDataIdentifier(int sensorId, String sensorType,@JsonIgnore Room room) implements Serializable {

    public int getSensorId() {
        return sensorId;
    }

    public String getSensorType() {
        return sensorType;
    }

    public Room getRoom() {
        return room;
    }
}
