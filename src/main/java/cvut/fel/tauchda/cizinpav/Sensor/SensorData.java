package cvut.fel.tauchda.cizinpav.Sensor;

import com.fasterxml.jackson.annotation.JsonIgnore;
import cvut.fel.tauchda.cizinpav.Device.Device;
import cvut.fel.tauchda.cizinpav.Entities.Room;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Map;

/**
 * Class use to wrap data from {@link Sensor}.
 */
public class SensorData implements Serializable {
    /**
     * Unique identifier of the {@link Sensor}.
     */
    private int idSensor;
    /**
     * Type of the {@link Sensor}.
     */
    private String type;
    /**
     * {@link Device} associated with {@link Sensor} (can be null)
     */
    @JsonIgnore
    private Device device;
    /**
     * {@link Room} where is the {@link Sensor} located.
     */
    @JsonIgnore
    private Room room;
    /**
     * {@link LocalDateTime} of when the data where captured (or if null when the data where send).
     */
    @JsonIgnore
    private LocalDateTime dateTime;
    /**
     * Data from {@link Sensor}.
     */
    @JsonIgnore
    private Map<String, Double> data;

    /**
     * Empty constructor
     */
    public SensorData() {
    }

    /**
     * Constructor without dateTime and Device.
     *
     * @param idSensor Unique identifier of the {@link Sensor}.
     * @param type     Type of the {@link Sensor}.
     * @param room     {@link Room} where is the {@link Sensor} located.
     * @param data     Data from {@link Sensor}.
     */
    public SensorData(int idSensor, String type, Room room, Map<String, Double> data) {
        this.idSensor = idSensor;
        this.type = type;
        this.device = null;
        this.room = room;
        this.dateTime = LocalDateTime.now();
        this.data = data;
    }

    /**
     * Constructor without Device.
     *
     * @param idSensor Unique identifier of the {@link Sensor}.
     * @param type     Type of the {@link Sensor}.
     * @param room     {@link Room} where is the {@link Sensor} located.
     * @param dateTime {@link LocalDateTime} of when the data where captured.
     * @param data     Data from {@link Sensor}.
     */
    public SensorData(int idSensor, String type, Room room, LocalDateTime dateTime, Map<String, Double> data) {
        this.idSensor = idSensor;
        this.type = type;
        this.device = null;
        this.room = room;
        this.dateTime = dateTime;
        this.data = data;
    }

    /**
     * Constructor without dateTime
     *
     * @param idSensor Unique identifier of the {@link Sensor}.
     * @param type     Type of the {@link Sensor}.
     * @param device   {@link Device} with which is {@link Sensor} associated.
     * @param room     {@link Room} where is the {@link Sensor} located.
     * @param data     Data from {@link Sensor}.
     */
    public SensorData(int idSensor, String type, Device device, Room room, Map<String, Double> data) {
        this.idSensor = idSensor;
        this.type = type;
        this.device = device;
        this.room = room;
        this.dateTime = LocalDateTime.now();
        this.data = data;
    }


    /**
     * Constructor
     *
     * @param idSensor Unique identifier of the {@link Sensor}.
     * @param type     Type of the {@link Sensor}.
     * @param device   {@link Device} with which is {@link Sensor} associated.
     * @param room     {@link Room} where is the {@link Sensor} located.
     * @param dateTime {@link LocalDateTime} of when the data where captured.
     * @param data     Data from {@link Sensor}.
     */
    public SensorData(int idSensor, String type, Device device, Room room, LocalDateTime dateTime, Map<String, Double> data) {
        this.idSensor = idSensor;
        this.type = type;
        this.device = device;
        this.room = room;
        this.dateTime = dateTime;
        this.data = data;
    }

    /**
     * {@link SensorData} id getter.
     *
     * @return Unique identifier of the {@link Sensor}.
     */
    public int getIdSensor() {
        return idSensor;
    }

    /**
     * {@link SensorData} id setter.
     *
     * @param idSensor Unique identifier of the {@link Sensor}.
     */
    public void setIdSensor(int idSensor) {
        this.idSensor = idSensor;
    }

    /**
     * {@link SensorData} type getter.
     *
     * @return Type of the {@link Sensor}.
     */
    public String getType() {
        return type;
    }

    /**
     * {@link SensorData} type setter.
     *
     * @param type Type of the {@link Sensor}.
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * {@link SensorData} device getter.
     *
     * @return {@link Device} with which is {@link Sensor} associated.
     */
    public Device getDevice() {
        return device;
    }

    /**
     * {@link SensorData} device setter.
     *
     * @param device {@link Device} with which is {@link Sensor} associated.
     */
    public void setDevice(Device device) {
        this.device = device;
    }

    /**
     * {@link SensorData} room getter.
     *
     * @return {@link Room} where is the {@link Sensor} located.
     */
    public Room getRoom() {
        return room;
    }

    /**
     * {@link SensorData} room setter.
     *
     * @param room {@link Room} where is the {@link Sensor} located.
     */
    public void setRoom(Room room) {
        this.room = room;
    }

    /**
     * {@link SensorData} dateTime getter.
     *
     * @return {@link LocalDateTime} of when the data where captured.
     */
    public LocalDateTime getDateTime() {
        return dateTime;
    }

    /**
     * {@link SensorData} dateTime setter.
     *
     * @param dateTime {@link LocalDateTime} of when the data where captured.
     */
    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }

    /**
     * {@link SensorData} data getter.
     *
     * @return Data from {@link Sensor}.
     */
    public Map<String, Double> getData() {
        return data;
    }

    /**
     * {@link SensorData} data setter.
     *
     * @param data Data from {@link Sensor}.
     */
    public void setData(Map<String, Double> data) {
        this.data = data;
    }

    public SensorDataIdentifier getSensorDataIdentifier() {
        return new SensorDataIdentifier(getIdSensor(), getType(), getRoom());
    }
}
