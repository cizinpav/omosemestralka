package cvut.fel.tauchda.cizinpav.Sensor;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;

public class SensorUpdateClock implements Serializable {
    private List<Sensor> sensors = new ArrayList<>();

    public SensorUpdateClock(List<Sensor> sensors) {
        this.sensors = sensors;
    }

    public void runOnce(){
        for (Sensor sensor:
                sensors) {
            sensor.update();
        }
    }

    public void run(){
        while(true){
            for (Sensor sensor:
                 sensors) {
                sensor.update();
            }
        }
    }

    public void run(int iterations){
        for (int i = 0; i < iterations; i++) {
            for (Sensor sensor:
                 sensors) {
                sensor.update();
            }
        }
    }

    public void run(long seconds){
        long now = LocalTime.now().toEpochSecond(LocalDate.now(), ZoneOffset.MIN);
        long stop = LocalTime.now().toEpochSecond(LocalDate.now(), ZoneOffset.MIN) + seconds;

        while(now<stop){
            for (Sensor sensor:
                 sensors)   {
                sensor.update();
            }
            now = LocalTime.now().toEpochSecond(LocalDate.now(), ZoneOffset.MIN);
        }
    }

    public void runWait(int sleepSeconds){
        while(true){
            for (Sensor sensor:
                    sensors) {
                sensor.update();
            }
            try {
                Thread.sleep(sleepSeconds * 1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void runWait(int iterations, int sleepSeconds){
        for (int i = 0; i < iterations; i++) {
            for (Sensor sensor:
                    sensors) {
                sensor.update();
            }
            try {
                Thread.sleep(sleepSeconds * 1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void runWait(long seconds, int sleepSeconds){
        long now = LocalTime.now().toEpochSecond(LocalDate.now(), ZoneOffset.MIN);
        long stop = LocalTime.now().toEpochSecond(LocalDate.now(), ZoneOffset.MIN) + seconds;

        while(now<stop){
            for (Sensor sensor:
                    sensors)   {
                sensor.update();
            }
            try {
                Thread.sleep(sleepSeconds * 1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            now = LocalTime.now().toEpochSecond(LocalDate.now(), ZoneOffset.MIN);
        }
    }
}
