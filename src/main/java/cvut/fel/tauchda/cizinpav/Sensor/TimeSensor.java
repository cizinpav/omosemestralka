package cvut.fel.tauchda.cizinpav.Sensor;

import cvut.fel.tauchda.cizinpav.Entities.Room;
import cvut.fel.tauchda.cizinpav.Subscriber;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.List;

public class TimeSensor extends Sensor{

    public TimeSensor(int idSensor, Room room) {
        super(idSensor, "Time sensor", room);
    }

    public TimeSensor(int idSensor, Room room, List<Subscriber> subscribers) {
        super(idSensor, "Time sensor", room, subscribers);
    }

    @Override
    public void update() {
        LocalDateTime time = LocalDateTime.now();
        updateData("Day", Double.valueOf(time.getDayOfMonth()));
        updateData("Month", Double.valueOf(time.getMonthValue()));
        updateData("Year", Double.valueOf(time.getYear()));
        updateData("Hour", Double.valueOf(time.getHour()));
        updateData("Minute", Double.valueOf(time.getMinute()));
        updateData("Second", Double.valueOf(time.getSecond()));
        updateData("Unix time", Double.valueOf(time.toEpochSecond(ZoneOffset.MIN)));

        notifySubscribers();
    }
}
