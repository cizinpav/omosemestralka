package cvut.fel.tauchda.cizinpav.Sensor;

import com.fasterxml.jackson.annotation.JsonIgnore;
import cvut.fel.tauchda.cizinpav.Device.Fridge;
import cvut.fel.tauchda.cizinpav.Entities.Room;
import cvut.fel.tauchda.cizinpav.Subscriber;

import java.util.List;
import java.util.Map;

/**
 * Subclass of {@link Sensor}.
 * Used to read quantity of food in a specific {@link Fridge}.
 */
public class FridgeSensor extends Sensor {
    /**
     * {@link Fridge} with which {@link FridgeSensor} is associated with.
     */
    @JsonIgnore
    private final Fridge fridge;

    /**
     * Constructor
     *
     * @param idSensor Unique identifier of {@link Sensor}.
     * @param room     {@link Room} where the {@link Sensor} is located.
     * @param fridge   {@link Fridge} with which {@link FridgeSensor} is associated with.
     */
    public FridgeSensor(int idSensor, Room room, Fridge fridge) {
        super(idSensor, "Fridge sensor", room);
        this.fridge = fridge;
    }

    /**
     * @param idSensor    Unique identifier of {@link Sensor}.
     * @param room        {@link Room} where the {@link Sensor} is located.
     * @param subscribers List of {@link Subscriber}s to notify when {@link Sensor} change state.
     * @param fridge      {@link Fridge} with which {@link FridgeSensor} is associated with.
     */
    public FridgeSensor(int idSensor, Room room, List<Subscriber> subscribers, Fridge fridge) {
        super(idSensor, "Fridge sensor", room, subscribers);
        this.fridge = fridge;
    }

    /**
     * Reads environment and notify {@link cvut.fel.tauchda.cizinpav.Subscriber}s of the change.
     */
    @Override
    public void update() {
        Map<String, Integer> fridgeContains = fridge.getContains();
        double sumOfContains = 0;
        for (double quantityOfFood :
                fridgeContains.values()) {
            sumOfContains += quantityOfFood; // Sum all quantities of all food in fridge.
        }

        updateData("food quantity", sumOfContains);
        updateData("fridge empty", 0.);

        if (sumOfContains == 0) {
            updateData("fridge empty", 1.);
        }

        notifySubscribers(); // Parse data into SensorData and updates subscribers with it.
    }
}
