package cvut.fel.tauchda.cizinpav.Sensor;

import com.fasterxml.jackson.annotation.JsonIgnore;
import cvut.fel.tauchda.cizinpav.Device.Device;
import cvut.fel.tauchda.cizinpav.Entities.Room;
import cvut.fel.tauchda.cizinpav.Subscriber;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.List;
import java.util.Random;

public class ConsumptionSensor extends Sensor {
    @JsonIgnore
    private Device device;
    private String lastStatus = "";

    public ConsumptionSensor(int idSensor, Room room, Device device) {
        super(idSensor, "Consumption sensor", room);
        this.device = device;
    }

    public ConsumptionSensor(int idSensor, Room room, List<Subscriber> subscribers, Device device) {
        super(idSensor, "Consumption sensor", room, subscribers);
        this.device = device;
    }

    @Override
    public void update() {
        if(device.getStatus().equals(lastStatus)){
            return;
        }
        lastStatus = device.getStatus();
        Random random = new Random();
        updateData("idDevice", Double.valueOf(device.getIdDevice()));
        updateData("UnixTime", Double.valueOf(LocalDateTime.now().toEpochSecond(ZoneOffset.MIN)));
        if(lastStatus.equals("Off")){
            updateData("deviceConsumption", 0.);
        } else {
            updateData("deviceConsumption", random.nextDouble(20, 400));
        }

        notifySubscribers();
    }
}
