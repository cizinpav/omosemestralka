package cvut.fel.tauchda.cizinpav.Sensor;

import cvut.fel.tauchda.cizinpav.Entities.Room;
import cvut.fel.tauchda.cizinpav.Subscriber;

import java.util.List;
import java.util.Random;

/**
 * Subclass of {@link Sensor}.
 * Used to read wind speed and angle.
 */
public class WindSensor extends Sensor {

    /**
     * @param idSensor Unique identifier of {@link Sensor}.
     * @param room     {@link Room} where the {@link Sensor} is located.
     *                 * @param room
     */
    public WindSensor(int idSensor, Room room) {
        super(idSensor, "Wind sensor", room);
    }

    /**
     * @param idSensor    Unique identifier of {@link Sensor}.
     * @param room        {@link Room} where the {@link Sensor} is located.
     * @param subscribers List of {@link Subscriber}s to notify when {@link Sensor} change state.
     */
    public WindSensor(int idSensor, Room room, List<Subscriber> subscribers) {
        super(idSensor, "Wind sensor", room, subscribers);
    }

    /**
     * Reads environment and notify {@link cvut.fel.tauchda.cizinpav.Subscriber}s of the change.
     */
    @Override
    public void update() {
        Random random = new Random();
        updateData("wind speed", random.nextDouble(0, 200));
        updateData("wind angle", random.nextDouble(0, 360));
        notifySubscribers();
    }
}
