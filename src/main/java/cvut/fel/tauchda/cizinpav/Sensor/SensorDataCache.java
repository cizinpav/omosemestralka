package cvut.fel.tauchda.cizinpav.Sensor;

import com.fasterxml.jackson.annotation.JsonIgnore;
import cvut.fel.tauchda.cizinpav.Entities.Room;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Cache for all {@link SensorData}.
 */
public class SensorDataCache implements Serializable {
    /**
     * Cache of all {@link SensorData} where key is {@link SensorDataIdentifier} and value is {@link SensorData}.
     */
    private Map<SensorDataIdentifier, SensorData> cache = new HashMap<>();

    /**
     * Empty constructor.
     */
    public SensorDataCache() {
    }

    /**
     * Constructor with cache.
     *
     * @param cache cache to be used.
     */
    public SensorDataCache(Map<SensorDataIdentifier, SensorData> cache) {
        this.cache = cache;
    }


    /**
     * Updates {@link SensorData} at {@link SensorDataIdentifier}.
     *
     * @param data {@link SensorData} at its` {@link SensorDataIdentifier}.
     */
    public void addData(SensorData data) {
        cache.put(data.getSensorDataIdentifier(), data);
    }

    /**
     * Remove all {@link SensorData} associated with {@link Sensor}.
     *
     * @param data {@link SensorData} from which {@link SensorDataIdentifier} is pulled.
     */
    public void removeData(SensorData data) {
        cache.remove(data.getSensorDataIdentifier());
    }

    /**
     * Remove all {@link SensorData} at {@link SensorDataIdentifier}.
     *
     * @param identifier {@link SensorDataIdentifier} to be removed.
     */
    public void removeDate(SensorDataIdentifier identifier) {
        cache.remove(identifier);
    }

    /**
     * Returns the latest {@link SensorData} from {@link Sensor} with specific id.
     *
     * @param sensorId id of the {@link Sensor} for which we want the latest {@link SensorData}.
     * @return Latest {@link SensorData} of {@link Sensor} identified by specific id.
     */
    public SensorData getDataById(int sensorId) {
        for (SensorDataIdentifier identifier :
                cache.keySet()) {
            if (identifier.getSensorId() == sensorId) {
                return cache.get(identifier);
            }
        }

        return null;
    }


    /**
     * Returns list of the latest {@link SensorData} from {@link Sensor} with specific sensorType.
     *
     * @param sensorType sensorType of the {@link Sensor} for which we want the latest {@link SensorData}.
     * @return List of latest {@link SensorData} of {@link Sensor} identified by specific sensorType.
     */
    public List<SensorData> getDataBySensorType(String sensorType) {
        List<SensorData> output = new ArrayList<>();
        for (SensorDataIdentifier identifier :
                cache.keySet()) {
            if (identifier.getSensorType().equals(sensorType)) {
                output.add(cache.get(identifier));
            }
        }

        return output;
    }

    /**
     * Returns list of the latest {@link SensorData} from {@link Sensor} with specific sensorType and {@link Room}.
     *
     * @param sensorType sensorType of the {@link Sensor} for which we want the latest {@link SensorData}.
     * @param room       {@link Room} where {@link Sensor} with specified sensorType have to be located
     * @return List of latest {@link SensorData} of {@link Sensor} identified by specific sensorType and {@link Room}.
     */
    public List<SensorData> getDataBySensorType(String sensorType, Room room) {
        List<SensorData> output = new ArrayList<>();
        for (SensorDataIdentifier identifier :
                cache.keySet()) {
            if (identifier.getSensorType().equals(sensorType) && identifier.getRoom() == room) {
                output.add(cache.get(identifier));
            }
        }

        return output;
    }

    public Map<SensorDataIdentifier, SensorData> getCache() {
        return cache;
    }

    public void setCache(Map<SensorDataIdentifier, SensorData> cache) {
        this.cache = cache;
    }
}
