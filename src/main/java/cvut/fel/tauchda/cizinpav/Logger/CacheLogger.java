package cvut.fel.tauchda.cizinpav.Logger;

import com.fasterxml.jackson.annotation.JsonIgnore;
import cvut.fel.tauchda.cizinpav.LoggerInterface;
import cvut.fel.tauchda.cizinpav.Sensor.SensorData;
import cvut.fel.tauchda.cizinpav.Sensor.SensorDataCache;
import cvut.fel.tauchda.cizinpav.Subscriber;

import java.io.Serializable;

/**
 * Logger used to update {@link SensorDataCache}.
 */
public class CacheLogger implements Subscriber, LoggerInterface {
    private String name = "CacheLogger";
    /**
     * Next class implementing {@link Subscriber} interface.
     */
    @JsonIgnore
    private Subscriber next;
    /**
     * Data received from {@link cvut.fel.tauchda.cizinpav.Sensor.Sensor} as {@link SensorData}.
     */
    @JsonIgnore
    private SensorData data;
    /**
     * {@link SensorDataCache} where data should be logged.
     */
    @JsonIgnore
    private SensorDataCache cache;

    /**
     * Minimal constructor
     *
     * @param cache {@link SensorDataCache} where data should be logged.
     */
    public CacheLogger(SensorDataCache cache) {
        this.cache = cache;
    }

    /**
     * Constructor
     *
     * @param next  Next class implementing {@link Subscriber} interface.
     * @param cache {@link SensorDataCache} where data should be logged.
     */
    public CacheLogger(Subscriber next, SensorDataCache cache) {
        this.next = next;
        this.cache = cache;
    }

    /**
     * Logs data to {@link SensorDataCache}.
     */
    @Override
    public void log() {
        cache.addData(data);
    }

    /**
     * Logs {@link SensorData} and send them to next {@link Subscriber}.
     *
     * @param data {@link SensorData} to be logged and send to next {@link Subscriber}.
     */
    @Override
    public void update(SensorData data) {
        this.data = data;
        log();
        if (next != null) {
            next.update(data);
        }
    }

    /**
     * Gets next {@link Subscriber} in chain of responsibility.
     *
     * @return Next {@link Subscriber} in chain of responsibility.
     */
    public Subscriber getNext() {
        return next;
    }

    /**
     * Sets next {@link Subscriber} in chain of responsibility.
     *
     * @param subscriber {@link Subscriber} to be next in chain of responsibility.
     */
    @Override
    public void setNext(Subscriber subscriber) {
        next = subscriber;
    }

    /**
     * {@link CacheLogger} data getter. ()
     *
     * @return {@link SensorData} stored in {@link CacheLogger} (there shouldn`t be reason to use this. Make class implementing {@link Subscriber} interface and use 'setNext(Subscriber subscriber)' to get receive data).
     */
    public SensorData getData() {
        return data;
    }

    /**
     * {@link CacheLogger} data setter.
     *
     * @param data {@link SensorData} to be stored in {@link CacheLogger} (there shouldn`t be reason to use this. If you want pass data make new {@link cvut.fel.tauchda.cizinpav.Sensor.Sensor} and add this to its` subscribers)
     */
    public void setData(SensorData data) {
        this.data = data;
    }

    /**
     * {@link CacheLogger} cache getter.
     *
     * @return {@link SensorDataCache} where this {@link CacheLogger} caches its` {@link SensorData}.
     */
    public SensorDataCache getCache() {
        return cache;
    }


    /**
     * {@link CacheLogger} cache setter.
     *
     * @param cache {@link SensorDataCache} where this {@link CacheLogger} should cache its` {@link SensorData}.
     */
    public void setCache(SensorDataCache cache) {
        this.cache = cache;
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
