package cvut.fel.tauchda.cizinpav.Logger;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.ObjectMapper;
import cvut.fel.tauchda.cizinpav.LoggerInterface;
import cvut.fel.tauchda.cizinpav.Sensor.SensorData;
import cvut.fel.tauchda.cizinpav.Subscriber;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ConsumptionLogger implements LoggerInterface, Subscriber {
    private String name = "ConsumptionLogger";
    /**
     * Data received from {@link cvut.fel.tauchda.cizinpav.Sensor.Sensor} as {@link SensorData}.
     */
    @JsonIgnore
    private SensorData data = null;
    /**
     * Next class implementing {@link Subscriber} interface.
     */
    @JsonIgnore
    private Subscriber next = null;

    private String logFileName = "consumptionReport";
    @JsonIgnore
    private File file;
    private boolean errorOccurred = false;
    private boolean freshFile = false;

    public ConsumptionLogger() {
        file = new File(logFileName+".json");
        if(!file.exists()){
            try {
                file.createNewFile();
            } catch (IOException e) {
                System.err.println("Can`t create file for consumption logging!");
                errorOccurred = true;
            }
        }
    }

    public ConsumptionLogger(String logFileName) {
        this.logFileName = logFileName;
        file = new File(logFileName+".json");
        if(!file.exists()){
            try {
                file.createNewFile();
            } catch (IOException e) {
                System.err.println("Can`t create file for consumption logging!");
                errorOccurred = true;
            }
        }
    }

    @Override
    public void setNext(Subscriber subscriber) {
        next = subscriber;
    }

    @Override
    public void update(SensorData data) {
        if(data.getType().equals("Consumption sensor")) {
            this.data = data;
            if (!errorOccurred) {
                log();
            }
        }
        if(next != null) {
            next.update(data);
        }
    }

    public SensorData getData() {
        return data;
    }

    public void setData(SensorData data) {
        this.data = data;
    }

    public Subscriber getNext() {
        return next;
    }

    public String getLogFileName() {
        return logFileName;
    }

    public void setLogFileName(String logFileName) {
        this.logFileName = logFileName;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isErrorOccurred() {
        return errorOccurred;
    }

    public void setErrorOccurred(boolean errorOccurred) {
        this.errorOccurred = errorOccurred;
    }

    public boolean isFreshFile() {
        return freshFile;
    }

    public void setFreshFile(boolean freshFile) {
        this.freshFile = freshFile;
    }

    @Override
    public void log() {
        ObjectMapper json = new ObjectMapper();

        Map<String, List<Map<String, Object>>> logData = new HashMap<>();
        List<Map<String, Object>> objectList = new ArrayList<>();
        Map<String, Object> logSpecificData = new HashMap<>();


        freshFile = file.length()==0 ? true : false;
        if(!freshFile) {
            try {
                logData = json.readValue(file, Map.class);
            } catch (IOException e) {
                System.err.println("Can`t read data in consumption logging!");
                errorOccurred = true;
                return;
            }
        }

        if(logData.get(String.valueOf(data.getData().get("idDevice")).split("\\.")[0]) != null) {
            objectList = logData.get(String.valueOf(data.getData().get("idDevice")).split("\\.")[0]);
        }

        logSpecificData.put("unix time", String.valueOf(data.getData().get("UnixTime")));
        logSpecificData.put("consumption", data.getData().get("deviceConsumption"));

        objectList.add(logSpecificData);
        logData.put(String.valueOf(data.getData().get("idDevice")).split("\\.")[0], objectList);

        try {
            json.writeValue(file, logData);
        } catch (IOException e) {
            System.err.println("Can`t write to consumption log file!");
            errorOccurred = true;
        }
    }
}
