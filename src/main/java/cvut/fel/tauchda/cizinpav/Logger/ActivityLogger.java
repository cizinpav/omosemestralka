package cvut.fel.tauchda.cizinpav.Logger;

import cvut.fel.tauchda.cizinpav.Controller.ControllerCaller;
import cvut.fel.tauchda.cizinpav.Device.Device;

import java.io.Serializable;

/**
 * Interface for {@link ActivityLogger}.
 * Classes implementing this interface should log actions and caller of the action.
 */
public interface ActivityLogger extends Serializable {
    String getLogFileName();

    void setLogFileName(String logFileName);


        /**
         * {@link Device} setter.
         * @param device {@link Device} which is target of action.
         */
    void setDevice(Device device);

    /**
     * {@link ControllerCaller} setter.
     * @param caller {@link ControllerCaller} which identifies the caller.
     */
    void setCaller(ControllerCaller caller);

    /**
     * Action setter
     * @param action String representation of action to be preformed on device.
     */
    void setAction(String action);

    /**
     * {@link Device} getter.
     * @return {@link Device}.
     */
    Device getDevice();

    /**
     * {@link ControllerCaller} getter.
     * @return {@link ControllerCaller}.
     */
    ControllerCaller getCaller();

    /**
     * Action getter.
     * @return {@link String}.
     */
    String getAction();

    /**
     * Log interaction with {@link Device}.
     */
    void log();

    /**
     * Log interaction with {@link Device}.
     *
     * @param device {@link Device} which is target of action.
     * @param caller {@link ControllerCaller} which identifies the caller.
     * @param action String representation of action to be preformed on device.
     */
    void log(Device device, ControllerCaller caller, String action);
}
