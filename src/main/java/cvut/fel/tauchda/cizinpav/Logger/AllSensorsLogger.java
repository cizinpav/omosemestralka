package cvut.fel.tauchda.cizinpav.Logger;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import cvut.fel.tauchda.cizinpav.LoggerInterface;
import cvut.fel.tauchda.cizinpav.Sensor.SensorData;
import cvut.fel.tauchda.cizinpav.Subscriber;

import java.io.Serializable;

/**
 * Logger for all {@link SensorData} from all {@link cvut.fel.tauchda.cizinpav.Sensor.Sensor}.
 *
 * @see cvut.fel.tauchda.cizinpav.LoggerInterface
 * @see cvut.fel.tauchda.cizinpav.Subscriber
 */
public class AllSensorsLogger implements LoggerInterface, Subscriber {
    private String name = "AllSensorsLogger";
    /**
     * Data received from {@link cvut.fel.tauchda.cizinpav.Sensor.Sensor} as {@link SensorData}.
     */
    @JsonIgnore
    private SensorData data = null;
    /**
     * Next class implementing {@link Subscriber} interface.
     */
    @JsonIgnore
    private Subscriber next = null;

    /**
     * Empty constructor
     */
    public AllSensorsLogger() {
    }

    /**
     * Constructor
     *
     * @param next {@link Subscriber} next in chain of responsibility.
     */
    public AllSensorsLogger(Subscriber next) {
        this.next = next;
    }

    /**
     * Logs {@link SensorData} and send them to next {@link Subscriber}.
     *
     * @param data {@link SensorData} to be logged and send to next {@link Subscriber}.
     */
    @Override
    public void update(SensorData data) {
        this.data = data;
        log();
        next.update(data);
    }

    /**
     * Parse {@link SensorData} as json and print it to standard output.
     */
    @Override
    public void log() {
        for (String key:
             data.getData().keySet()) {
            System.out.print(key + ": " + data.getData().get(key)+" ");
        }
        System.out.println();
    }

    /**
     * {@link AllSensorsLogger} data getter.
     *
     * @return {@link SensorData} stored in {@link AllSensorsLogger} (there shouldn`t be reason to use this. Make class implementing {@link Subscriber} interface and use 'setNext(Subscriber subscriber)' to get receive data)
     */
    public SensorData getData() {
        return data;
    }

    /**
     * {@link AllSensorsLogger} data setter.
     *
     * @param data {@link SensorData} to be stored in {@link AllSensorsLogger} (there shouldn`t be reason to use this. If you want pass data make new {@link cvut.fel.tauchda.cizinpav.Sensor.Sensor} and add this to its` subscribers)
     */
    public void setData(SensorData data) {
        this.data = data;
    }

    /**
     * {@link AllSensorsLogger} next getter.
     *
     * @return Next {@link Subscriber} in chain.
     */
    public Subscriber getNext() {
        return next;
    }

    /**
     * Sets next {@link Subscriber} in chain of responsibility.
     *
     * @param subscriber {@link Subscriber} to be next in chain of responsibility.
     */
    @Override
    public void setNext(Subscriber subscriber) {
        next = subscriber;
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
