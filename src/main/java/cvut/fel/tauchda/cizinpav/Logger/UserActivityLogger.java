package cvut.fel.tauchda.cizinpav.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;
import cvut.fel.tauchda.cizinpav.Controller.ControllerCaller;
import cvut.fel.tauchda.cizinpav.Device.Device;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UserActivityLogger implements ActivityLogger {
    private Device device;
    private ControllerCaller caller;
    private String action;
    private String logFileName = "activityReport";
    private boolean errorOccurred = false;
    private static boolean isInitialized = false;
    private static UserActivityLogger instance = null;

    public static UserActivityLogger getInstance(){
        if(!isInitialized){
            instance = new UserActivityLogger();
            isInitialized = true;
        }

        return instance;
    }

    public static UserActivityLogger getInstance(String fileName){
        if(!isInitialized){
            instance = new UserActivityLogger(fileName);
            isInitialized = true;
        }

        return instance;
    }

    private UserActivityLogger() {
    }

    private UserActivityLogger(String logFileName) {
        this.logFileName = logFileName;
    }

    @Override
    public String getLogFileName() {
        return logFileName;
    }

    @Override
    public void setLogFileName(String logFileName) {
        this.logFileName = logFileName;
    }

    @Override
    public Device getDevice() {
        return device;
    }

    @Override
    public void setDevice(Device device) {
        this.device = device;
    }

    @Override
    public ControllerCaller getCaller() {
        return caller;
    }

    @Override
    public void setCaller(ControllerCaller caller) {
        this.caller = caller;
    }

    @Override
    public String getAction() {
        return action;
    }

    @Override
    public void setAction(String action) {
        this.action = action;
    }

    @Override
    public void log() {
        if(errorOccurred){
            return;
        }
        ObjectMapper json = new ObjectMapper();
        File file = new File(logFileName+".json");

        Map<String, List<Map<String, Object>>> logData = null;
        List<Map<String, Object>> objectList = null;

        if(!file.exists()){
            try {
                file.createNewFile();
                logData = new HashMap<>();
                objectList = new ArrayList<>();
            } catch (IOException e) {
                System.err.println("Can't create file to log activity into!");
                errorOccurred = true;
                return;
            }
        } else {
            try {
                logData = json.readValue(file, Map.class);
                objectList = logData.get(caller.getCallerOwner());
                if(objectList == null){
                    objectList = new ArrayList<>();
                }
            } catch (IOException e) {
                logData = new HashMap<>();
                objectList = new ArrayList<>();
            }
        }

        Map<String, Object> logSpecificData = new HashMap<>();
        logSpecificData.put("Time", LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss")));
        logSpecificData.put("CallerType", caller.getCallerType());
        logSpecificData.put("IdDevice", device.getIdDevice());
        logSpecificData.put("NameDevice", device.getName());
        logSpecificData.put("Action", action);

        objectList.add(logSpecificData);

        logData.put(caller.getCallerOwner(), objectList);
        try {
            json.writeValue(file, logData);
        } catch (IOException e) {
            System.err.println("Can't convert data to json!");
        }
    }

    @Override
    public void log(Device device, ControllerCaller caller, String action) {
        setDevice(device);
        setCaller(caller);
        setAction(action);
        log();
    }
}
