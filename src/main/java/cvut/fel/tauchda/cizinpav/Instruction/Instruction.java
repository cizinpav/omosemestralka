package cvut.fel.tauchda.cizinpav.Instruction;

import com.fasterxml.jackson.annotation.JsonIgnore;
import cvut.fel.tauchda.cizinpav.DeviceCommands.DeviceCommand;
import cvut.fel.tauchda.cizinpav.Instruction.ExecutionConstraints.ExecutionConstraint;
import cvut.fel.tauchda.cizinpav.Entities.Room;
import cvut.fel.tauchda.cizinpav.Logger.ActivityLogger;
import cvut.fel.tauchda.cizinpav.Sensor.SensorData;
import cvut.fel.tauchda.cizinpav.Sensor.SensorDataCache;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * This class executes a list of {@link DeviceCommand} when all {@link ExecutionConstraint} are satisfied.
 */
public class Instruction implements Serializable {
    /**
     * Unique identifier of the {@link Instruction}.
     */
    private final int idInstruction;
    /**
     * Name of the {@link Instruction} chosen by user (Preferably unique but not necessary).
     */
    private String name;
    /**
     * List of {@link ExecutionConstraint} to check if the {@link Instruction} should execute its commands
     */
    private List<ExecutionConstraint> constraints = new ArrayList<>();
    /**
     * List of {@link DeviceCommand} to be executed.
     */
    private List<DeviceCommand> commands = new ArrayList<>();

    @JsonIgnore
    private SensorDataCache cache;


    /**
     * Minimal constructor
     *
     * @param idInstruction Unique identifier of the {@link Instruction}.
     * @param name          Name of the {@link Instruction}.
     */
    public Instruction(int idInstruction, String name, SensorDataCache cache) {
        this.idInstruction = idInstruction;
        this.cache = cache;
        this.name = name;
    }


    /**
     * Small constructor
     *
     * @param idInstruction Unique identifier of the {@link Instruction}.
     * @param name          Name of the {@link Instruction}.
     * @param constraint    {@link ExecutionConstraint} that needs to be satisfied to run command
     * @param command       {@link DeviceCommand} to be run if the constraint is satisfied.
     */
    public Instruction(int idInstruction, String name, ExecutionConstraint constraint, DeviceCommand command, SensorDataCache cache) {
        this.idInstruction = idInstruction;
        this.name = name;
        this.constraints.add(constraint);
        this.commands.add(command);
        this.cache = cache;
    }


    /**
     * Standard constructor
     *
     * @param idInstruction Unique identifier of the {@link Instruction}.
     * @param name          Name of the {@link Instruction}.
     * @param constraints   List of {@link ExecutionConstraint} where all needs to be satisfied to run commands
     * @param commands      List of {@link DeviceCommand} to be run if all constraints are satisfied.
     */
    public Instruction(int idInstruction, String name, List<ExecutionConstraint> constraints, List<DeviceCommand> commands, SensorDataCache cache) {
        this.idInstruction = idInstruction;
        this.name = name;
        this.constraints = constraints;
        this.commands = commands;
        this.cache = cache;
    }

    /**
     * Takes {@link SensorData} from {@link SensorDataCache} and check if it satisfies all constraint.
     *
     * @return True if all constraints are satisfied. Else false.
     */
    public boolean shouldBeExecuted() {
        int idSensor;
        String sensorType;
        Room room;
        for (ExecutionConstraint constraint :
                constraints) {
            idSensor = constraint.getIdSensor();
            sensorType = constraint.getSensorType();
            room = constraint.getRoom();

            if (idSensor != -1) {
                if (cache.getDataById(idSensor) != null) {
                    if (!constraint.checkConstraint(cache.getDataById(idSensor))) {
                        return false;
                    }
                }
            } else if (sensorType != null) {
                if (room != null) {
                    if (cache.getDataBySensorType(sensorType, room) != null) {
                        if (!constraint.checkConstraint(cache.getDataBySensorType(sensorType, room))) {
                            return false;
                        }
                    }
                } else {
                    if (cache.getDataBySensorType(sensorType) != null) {
                        if (!constraint.checkConstraint(cache.getDataBySensorType(sensorType))) {
                            return false;
                        }
                    }
                }
            }
        }

        return true;
    }


    /**
     * Check if {@link SensorData} satisfies all constraint
     *
     * @param data {@link SensorData} to be checked.
     * @return True if all constraints are satisfied. Else false.
     */
    public boolean shouldBeExecuted(SensorData data) {
        for (ExecutionConstraint constraint :
                constraints) {
            if (!constraint.checkConstraint(data)) { // Checks if constraint is satisfied.
                return false;
            }
        }

        return true;
    }

    /**
     * Execute all {@link DeviceCommand} ordered by index in list.
     */
    public void execute() {
        for (DeviceCommand command :
                commands) {
            command.execute();
        }
    }

    public void execute(ActivityLogger logger){
        for (DeviceCommand command:
        commands) {
            logger.setDevice(command.getDevice());
            logger.setAction(command.getAction());
            logger.log();
            command.execute();
        }
    }

    /**
     * {@link Instruction} id getter.
     *
     * @return unique identifier of the {@link Instruction}.
     */
    public int getIdInstruction() {
        return idInstruction;
    }

    /**
     * {@link Instruction} name getter.
     *
     * @return name of the {@link Instruction}.
     */
    public String getName() {
        return name;
    }

    /**
     * {@link Instruction} name setter.
     *
     * @param name name to be set as {@link Instruction} name.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * {@link Instruction} constrains getter.
     *
     * @return List of all the {@link Instruction} {@link ExecutionConstraint}s.
     */
    public List<ExecutionConstraint> getConstraints() {
        return constraints;
    }

    /**
     * {@link Instruction} constrains setter.
     *
     * @param constraints List of {@link ExecutionConstraint} to be set.
     */
    public void setConstraints(List<ExecutionConstraint> constraints) {
        this.constraints = constraints;
    }

    /**
     * Adds {@link ExecutionConstraint} to be checked by the {@link Instruction}.
     *
     * @param constraint {@link ExecutionConstraint} to be added to the list of constraints.
     */
    public void addConstraint(ExecutionConstraint constraint) {
        this.constraints.add(constraint);
    }

    /**
     * Remove {@link ExecutionConstraint} from the {@link Instruction} constraints list.
     *
     * @param constraint {@link ExecutionConstraint} to be removed from the list.
     */
    public void removeConstraint(ExecutionConstraint constraint) {
        this.constraints.remove(constraint);
    }

    /**
     * {@link Instruction} commands getter.
     *
     * @return List of {@link DeviceCommand} from the {@link Instruction}.
     */
    public List<DeviceCommand> getCommands() {
        return commands;
    }

    /**
     * {@link Instruction} commands setter.
     *
     * @param commands List of {@link DeviceCommand} to be executed by {@link Instruction}.
     */
    public void setCommands(List<DeviceCommand> commands) {
        this.commands = commands;
    }

    /**
     * Adds {@link DeviceCommand} to the list of commands
     *
     * @param command {@link DeviceCommand} to be added at the end of {@link Instruction}`s list of commands.
     */
    public void addCommand(DeviceCommand command) {
        this.commands.add(command);
    }

    /**
     * Adds {@link DeviceCommand} to the list of commands on specific index.
     *
     * @param command {@link DeviceCommand} to be added at the index of {@link Instruction}`s list of commands.
     * @param index   index where should the command reside in list of commands.
     */
    public void addCommand(DeviceCommand command, int index) {
        this.commands.add(index, command);
    }

    /**
     * Removes {@link DeviceCommand} from the list of commands.
     *
     * @param command {@link DeviceCommand} to be removed from {@link Instruction}`s list of commands.
     */
    public void removeCommand(DeviceCommand command) {
        this.commands.remove(command);
    }

    /**
     * {@link Instruction} cache getter.
     *
     * @return {@link SensorDataCache} used to check constrains.
     */
    public SensorDataCache getCache() {
        return cache;
    }

    /**
     * {@link Instruction} cache setter.
     *
     * @param cache {@link SensorDataCache} to be used to check constrains.
     */
    public void setCache(SensorDataCache cache) {
        this.cache = cache;
    }
}
