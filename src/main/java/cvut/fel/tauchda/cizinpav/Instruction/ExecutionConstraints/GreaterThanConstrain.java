package cvut.fel.tauchda.cizinpav.Instruction.ExecutionConstraints;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import cvut.fel.tauchda.cizinpav.Entities.Room;
import cvut.fel.tauchda.cizinpav.Sensor.SensorData;

import java.util.List;

public class GreaterThanConstrain implements ExecutionConstraint {
    private final String sensorType;
    @JsonProperty
    private final String key;
    @JsonProperty
    private final double value;
    private final Room room;
    private final int idSensor;

    public GreaterThanConstrain(String sensorType, int idSensor, Room room, String key, double value) {
        this.sensorType = sensorType;
        this.idSensor = idSensor;
        this.value = value;
        this.room = room;
        this.key = key;
    }


    @Override
    public boolean checkConstraint(SensorData data) {
        if (data.getData().isEmpty()) {
            return false;
        }

        if (data.getIdSensor() == idSensor) {
            return data.getData().get(key) > value;
        }

        return false;
    }

    @Override
    public boolean checkConstraint(List<SensorData> dataList) {
        for (SensorData data : dataList) {
            if (data.getType().equals(sensorType)) {
                if (room != null) {
                    if (data.getRoom() == room) {
                        if (data.getData().get(key) > value) {
                            return true;
                        }
                    }
                } else {
                    if (data.getData().get(key) > value) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    @Override
    public String getSensorType() {
        return sensorType;
    }

    @Override
    public Room getRoom() {
        return room;
    }

    @Override
    public int getIdSensor() {
        return idSensor;
    }

    public String getKey() {
        return key;
    }

    public double getValue() {
        return value;
    }
}
