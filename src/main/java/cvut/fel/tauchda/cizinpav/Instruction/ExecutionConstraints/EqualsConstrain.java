package cvut.fel.tauchda.cizinpav.Instruction.ExecutionConstraints;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import cvut.fel.tauchda.cizinpav.Entities.Room;
import cvut.fel.tauchda.cizinpav.Sensor.SensorData;

import java.util.List;

/**
 * Subclass of {@link ExecutionConstraint} for checking equality/inequality.
 */
public class EqualsConstrain implements ExecutionConstraint {
    /**
     * sensorType we want to compare against.
     */
    private final String sensorType;
    /**
     * If true then constraint checks inequality. Else checks equality.
     */
    @JsonProperty
    private final boolean reversed;
    /**
     * Key to compare {@link SensorData}.data
     */
    @JsonProperty
    private final String key;
    /**
     * Value to test equality/inequality against.
     */
    @JsonProperty
    private final double value;
    /**
     * room where we want to find sensorType (if null then all {@link cvut.fel.tauchda.cizinpav.Sensor.Sensor}s with sensorType are checked).
     */
    private final Room room;
    /**
     * unique {@link cvut.fel.tauchda.cizinpav.Sensor.Sensor} identifier to be checked against.
     * (if this is -1 than don`t check against idSensor. Else have higher priority than sensorType)
     */
    private final int idSensor;

    public EqualsConstrain(String sensorType, int idSensor, Room room, String key, double value, boolean reversed) {
        this.sensorType = sensorType;
        this.idSensor = idSensor;
        this.reversed = reversed;
        this.value = value;
        this.room = room;
        this.key = key;
    }


    @Override
    public boolean checkConstraint(SensorData data) {
        if (data.getData().isEmpty()) {
            return false;
        }

        if (data.getIdSensor() == idSensor) {
            return (data.getData().get(key) == value ^ reversed);
        }

        return false;
    }

    public boolean checkConstraint(List<SensorData> dataList) {
        for (SensorData data : dataList) {
            if (data.getType().equals(sensorType)) {
                if (room != null) {
                    if (data.getRoom() == room) {
                        if ((data.getData().get(key) == value ^ reversed)) {
                            return true;
                        }
                    }
                } else {
                    if ((data.getData().get(key) == value ^ reversed)) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    public String getSensorType() {
        return sensorType;
    }

    public Room getRoom() {
        return room;
    }

    public int getIdSensor() {
        return idSensor;
    }

    public boolean isReversed() {
        return reversed;
    }

    public String getKey() {
        return key;
    }

    public double getValue() {
        return value;
    }
}
