package cvut.fel.tauchda.cizinpav.Instruction.ExecutionConstraints;

import cvut.fel.tauchda.cizinpav.Entities.Room;

/**
 * Factory for making {@link ExecutionConstraint}
 */
public class ExecutionConstrainFactory {
    /**
     * sensor type to check against.
     */
    private String sensorType;
    /**
     * unique identifier of the {@link cvut.fel.tauchda.cizinpav.Sensor.Sensor} to be check against.
     */
    private int idSensor = -1;
    /**
     * used with sensorType to check only in specific {@link Room}.
     */
    private Room room;

    /**
     * Empty constructor
     * Before use must set idSensor or sensorType or (sensorType and room).
     */
    public ExecutionConstrainFactory() {
    }

    /**
     * Constructor
     *
     * @param sensorType sensorType to check against.
     */
    public ExecutionConstrainFactory(String sensorType) {
        this.sensorType = sensorType;
    }

    /**
     * Constructor
     *
     * @param idSensor unique identifier of the {@link cvut.fel.tauchda.cizinpav.Sensor.Sensor} to check against.
     */
    public ExecutionConstrainFactory(int idSensor) {
        this.idSensor = idSensor;
    }

    /**
     * Constructor
     *
     * @param sensorType sensorType to check against.
     * @param room       {@link Room} where {@link cvut.fel.tauchda.cizinpav.Sensor.Sensor} must reside.
     */
    public ExecutionConstrainFactory(String sensorType, Room room) {
        this.sensorType = sensorType;
        this.room = room;
    }

    /**
     * Makes {@link ExecutionConstraint} where passed value needs to equal the value in {@link cvut.fel.tauchda.cizinpav.Sensor.SensorData}.data at key to be satisfied.
     *
     * @param key   key in {@link cvut.fel.tauchda.cizinpav.Sensor.SensorData}.data to be checked against.
     * @param value value which check equality.
     * @return {@link EqualsConstrain}.
     */
    public ExecutionConstraint getEqualsConstrain(String key, double value) {
        return new EqualsConstrain(sensorType, idSensor, room, key, value, false);
    }

    /**
     * Makes {@link ExecutionConstraint} where passed value needs to be different the value in {@link cvut.fel.tauchda.cizinpav.Sensor.SensorData}.data at key to be satisfied.
     *
     * @param key   key in {@link cvut.fel.tauchda.cizinpav.Sensor.SensorData}.data to be checked against.
     * @param value value which check inequality.
     * @return {@link EqualsConstrain}.
     */
    public ExecutionConstraint getNotEqualsConstrain(String key, double value) {
        return new EqualsConstrain(sensorType, idSensor, room, key, value, true);
    }

    /**
     * Makes {@link ExecutionConstraint} where value in {@link cvut.fel.tauchda.cizinpav.Sensor.SensorData}.data at key needs to be bigger or equal to minValue and smaller or equal to maxValue to be satisfied.
     *
     * @param key      key in {@link cvut.fel.tauchda.cizinpav.Sensor.SensorData}.data to be checked against.
     * @param minValue lower bound of checked interval (inclusive)
     * @param maxValue upper bound of checked interval (inclusive)
     * @return {@link EqualsRangeConstrain}.
     */
    public ExecutionConstraint getEqualsRangeConstrain(String key, double minValue, double maxValue) {
        return new EqualsRangeConstrain(sensorType, idSensor, room, key, minValue, maxValue, false);
    }

    /**
     * Makes {@link ExecutionConstraint} where value in {@link cvut.fel.tauchda.cizinpav.Sensor.SensorData}.data at key needs to be smaller than minValue or bigger than maxValue to be satisfied.
     *
     * @param key      key in {@link cvut.fel.tauchda.cizinpav.Sensor.SensorData}.data to be checked against.
     * @param minValue lower bound of checked interval (inclusive)
     * @param maxValue upper bound of checked interval (inclusive)
     * @return {@link EqualsRangeConstrain}.
     */
    public ExecutionConstraint getNotEqualsRangeConstrain(String key, double minValue, double maxValue) {
        return new EqualsRangeConstrain(sensorType, idSensor, room, key, minValue, maxValue, true);
    }


    /**
     * Makes {@link ExecutionConstraint} where passed value needs to be bigger than value in {@link cvut.fel.tauchda.cizinpav.Sensor.SensorData}.data at key to be satisfied.
     *
     * @param key   key in {@link cvut.fel.tauchda.cizinpav.Sensor.SensorData}.data to be checked against.
     * @param value value to compare {@link cvut.fel.tauchda.cizinpav.Sensor.SensorData}.data at key against.
     * @return {@link LessThanConstrain}
     */
    public ExecutionConstraint getLessThanConstrain(String key, double value) {
        return new LessThanConstrain(sensorType, idSensor, room, key, value);
    }

    /**
     * Makes {@link ExecutionConstraint} where passed value needs to be smaller than value in {@link cvut.fel.tauchda.cizinpav.Sensor.SensorData}.data at key to be satisfied.
     *
     * @param key   key in {@link cvut.fel.tauchda.cizinpav.Sensor.SensorData}.data to be checked against.
     * @param value value to compare {@link cvut.fel.tauchda.cizinpav.Sensor.SensorData}.data at key against.
     * @return {@link LessThanConstrain}
     */
    public ExecutionConstraint getGreaterThanConstrain(String key, double value) {
        return new GreaterThanConstrain(sensorType, idSensor, room, key, value);
    }

    /**
     * {@link ExecutionConstrainFactory} sensorType getter.
     *
     * @return sensorType used to check against.
     */
    public String getSensorType() {
        return sensorType;
    }

    /**
     * {@link ExecutionConstrainFactory} sensorType setter.
     *
     * @param sensorType sensorType to check against.
     */
    public void setSensorType(String sensorType) {
        this.sensorType = sensorType;
        this.idSensor = -1; // When we are looking for specific sensorType we don`t want to look for one specific.
    }

    /**
     * {@link ExecutionConstrainFactory} idSensor getter.
     *
     * @return unique identifier of the {@link cvut.fel.tauchda.cizinpav.Sensor.Sensor} used to check against.
     */
    public int getIdSensor() {
        return idSensor;
    }


    /**
     * {@link ExecutionConstrainFactory} idSensor setter.
     *
     * @param idSensor unique {@link cvut.fel.tauchda.cizinpav.Sensor.Sensor} identifier to check against.
     */
    public void setIdSensor(int idSensor) {
        this.idSensor = idSensor; // When Execution constrains checks their conditions' id is higher than sensorType so we don't have to change it
    }

    /**
     * {@link ExecutionConstrainFactory} room getter.
     *
     * @return {@link Room} where {@link cvut.fel.tauchda.cizinpav.Sensor.Sensor} with wanted sensorType is located.
     */
    public Room getRoom() {
        return room;
    }

    /**
     * {@link ExecutionConstrainFactory} room setter.
     *
     * @param room {@link Room} where {@link cvut.fel.tauchda.cizinpav.Sensor.Sensor} with wanted sensorType is located.
     */
    public void setRoom(Room room) {
        this.room = room;
        this.idSensor = -1; // When we are looking for specific sensorType and room we don`t want to look for one specific.
    }
}
