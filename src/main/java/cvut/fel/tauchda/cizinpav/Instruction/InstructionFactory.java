package cvut.fel.tauchda.cizinpav.Instruction;

import cvut.fel.tauchda.cizinpav.Instruction.ExecutionConstraints.ExecutionConstraint;
import cvut.fel.tauchda.cizinpav.DeviceCommands.DeviceCommand;
import cvut.fel.tauchda.cizinpav.Sensor.SensorDataCache;

import java.util.List;

public class InstructionFactory {
    private int newIdInstruction = -1;
    private final SensorDataCache cache;

    public InstructionFactory(SensorDataCache cache) {
        this.cache = cache;
    }

    public Instruction getNewInstruction(){
        newIdInstruction++;
        return new Instruction(newIdInstruction, "Unnamed", cache);
    }

    public Instruction getNewInstruction(List<ExecutionConstraint> constraints, List<DeviceCommand> commands){
        newIdInstruction++;
        return new Instruction(newIdInstruction, "Unnamed", constraints, commands, cache);
    }

    public Instruction getNewInstruction(String name, List<ExecutionConstraint> constraints, List<DeviceCommand> commands){
        newIdInstruction++;
        return new Instruction(newIdInstruction, name, constraints, commands, cache);
    }
}
