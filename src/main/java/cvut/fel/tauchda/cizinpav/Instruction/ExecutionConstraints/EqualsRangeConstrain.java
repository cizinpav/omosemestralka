package cvut.fel.tauchda.cizinpav.Instruction.ExecutionConstraints;

import com.fasterxml.jackson.annotation.JsonProperty;
import cvut.fel.tauchda.cizinpav.Entities.Room;
import cvut.fel.tauchda.cizinpav.Sensor.SensorData;

import java.util.List;

public class EqualsRangeConstrain implements ExecutionConstraint {
    private final String sensorType;
    @JsonProperty
    private final boolean reversed;
    @JsonProperty
    private final String key;
    @JsonProperty
    private final double minValue;
    @JsonProperty
    private final double maxValue;
    private final Room room;
    private final int idSensor;

    public EqualsRangeConstrain(String sensorType, int idSensor, Room room, String key, double minValue, double maxValue, boolean reversed) {
        this.sensorType = sensorType;
        this.idSensor = idSensor;
        this.reversed = reversed;
        this.minValue = minValue;
        this.maxValue = maxValue;
        this.room = room;
        this.key = key;
    }


    @Override
    public boolean checkConstraint(SensorData data) {
        if (data.getData().isEmpty()) {
            return false;
        }

        if (data.getIdSensor() == idSensor) {
            return ((data.getData().get(key) >= minValue && data.getData().get(key) <= maxValue) ^ reversed);
        }

        return false;
    }

    public boolean checkConstraint(List<SensorData> dataList) {
        for (SensorData data : dataList) {
            if (data.getType().equals(sensorType)) {
                if (room != null) {
                    if (data.getRoom() == room) {
                        if ((data.getData().get(key) >= minValue && data.getData().get(key) <= maxValue) ^ reversed) {
                            return true;
                        }
                    }
                } else {
                    if ((data.getData().get(key) >= minValue && data.getData().get(key) <= maxValue) ^ reversed) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    @Override
    public String getSensorType() {
        return sensorType;
    }

    @Override
    public Room getRoom() {
        return room;
    }

    @Override
    public int getIdSensor() {
        return idSensor;
    }

    public boolean isReversed() {
        return reversed;
    }

    public String getKey() {
        return key;
    }

    public double getMinValue() {
        return minValue;
    }

    public double getMaxValue() {
        return maxValue;
    }
}
