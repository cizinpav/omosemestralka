package cvut.fel.tauchda.cizinpav.Instruction.ExecutionConstraints;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import cvut.fel.tauchda.cizinpav.Instruction.Instruction;
import cvut.fel.tauchda.cizinpav.Entities.Room;
import cvut.fel.tauchda.cizinpav.Sensor.SensorData;

import java.io.Serializable;
import java.util.List;

/**
 * Interface for {@link ExecutionConstraint} used in {@link Instruction}.
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME)
public interface ExecutionConstraint extends Serializable {
    boolean checkConstraint(SensorData data);

    boolean checkConstraint(List<SensorData> dataList);

    @JsonIgnore
    Room getRoom();

    int getIdSensor();

    String getSensorType();
}
