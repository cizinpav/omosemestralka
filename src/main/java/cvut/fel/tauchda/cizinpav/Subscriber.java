package cvut.fel.tauchda.cizinpav;

import cvut.fel.tauchda.cizinpav.Sensor.SensorData;

import java.io.Serializable;

public interface Subscriber extends Serializable {
    void setNext(Subscriber subscriber);

    String getName();

    void update(SensorData data);
}
