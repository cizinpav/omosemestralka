package cvut.fel.tauchda.cizinpav.Controller;

import cvut.fel.tauchda.cizinpav.Device.AlarmClock;
import cvut.fel.tauchda.cizinpav.DeviceCommands.DeviceCommand;
import cvut.fel.tauchda.cizinpav.Instruction.ExecutionConstraints.ExecutionConstrainFactory;
import cvut.fel.tauchda.cizinpav.Instruction.ExecutionConstraints.ExecutionConstraint;
import cvut.fel.tauchda.cizinpav.Instruction.Instruction;
import cvut.fel.tauchda.cizinpav.Instruction.InstructionFactory;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.List;
import java.util.Map;

/**
 * Subclass of {@link ManualController} for {@link AlarmClock}.
 */
public class AlarmClockController extends ManualController{
    /**
     * Constructor
     *
     * @param alarmClock {@link AlarmClock} for {@link AlarmClockController} to be associated with.
     */
    public AlarmClockController(AlarmClock alarmClock) {
        super(alarmClock);
    }

    /**
     * Extended constructor
     * @param alarmClock {@link AlarmClock} for {@link AlarmClockController} to be associated with.
     * @param commands {@link DeviceCommand} associated with {@link AlarmClock}.
     */
    public AlarmClockController(AlarmClock alarmClock, Map<String, DeviceCommand> commands) {
        super(alarmClock, commands);
    }

    /**
     * Make {@link Instruction} that will ring an {@link AlarmClock}.
     * @param instructionFactory {@link InstructionFactory} used to make {@link Instruction}.
     * @param time {@link LocalDateTime} when should the {@link AlarmClock} ring.
     * @return {@link Instruction} that will execute {@link DeviceCommand} which will ring {@link AlarmClock} at specified time.
     */
    public Instruction setAlarm(InstructionFactory instructionFactory, LocalDateTime time) {
        ExecutionConstrainFactory constrainFactory = new ExecutionConstrainFactory("Time sensor");
        ExecutionConstraint unixTimeConstraint = constrainFactory.getGreaterThanConstrain("Unix time", Double.valueOf(time.toEpochSecond(ZoneOffset.MIN)));

        Instruction instruction = instructionFactory.getNewInstruction();
        instruction.addConstraint(unixTimeConstraint);
        instruction.addCommand(this.getCommands().get("AlarmClockRing"));

        return instruction;
    }

    /**
     * Add {@link ExecutionConstraint} and {@link DeviceCommand} to ring an {@link AlarmClock} at a specified time.
     * @param instruction {@link Instruction} to which should be the {@link ExecutionConstraint} and {@link DeviceCommand} added to. (Preferably instruction should be empty).
     * @param time {@link LocalDateTime} when should the {@link AlarmClock} ring.
     */
    public void setAlarm(Instruction instruction, LocalDateTime time) {
        ExecutionConstrainFactory constrainFactory = new ExecutionConstrainFactory("Time sensor");
        ExecutionConstraint unixTimeConstraint = constrainFactory.getGreaterThanConstrain("Unix time", Double.valueOf(time.toEpochSecond(ZoneOffset.MIN)));

        instruction.addConstraint(unixTimeConstraint);
        instruction.addCommand(this.getCommands().get("AlarmClockRing"));
    }

    /**
     * Execute command to stop ringing an {@link AlarmClock}.
     */
    public void snoozeAlarm(){
       this.getCommands().get("AlarmClockSnooze").execute();
    }
}
