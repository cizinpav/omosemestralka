package cvut.fel.tauchda.cizinpav.Controller;

import com.fasterxml.jackson.annotation.JsonIgnore;
import cvut.fel.tauchda.cizinpav.Instruction.Instruction;
import cvut.fel.tauchda.cizinpav.Logger.ActivityLogger;
import cvut.fel.tauchda.cizinpav.Logger.UserActivityLogger;
import cvut.fel.tauchda.cizinpav.Sensor.SensorData;
import cvut.fel.tauchda.cizinpav.Subscriber;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class AutomatedController implements Subscriber, Serializable {
    private String name = "AutomatedController";
    private List<Instruction> instructions = new ArrayList<>();
    @JsonIgnore
    private final ActivityLogger activityLogger = UserActivityLogger.getInstance();
    @JsonIgnore
    private Subscriber next;

    public AutomatedController() {
        activityLogger.setCaller(new ControllerCaller() {
            @Override
            public String getCallerType() {
                return "Instruction";
            }

            @Override
            public String getCallerOwner() {
                return "AutomatedController";
            }
        });
    }

    public AutomatedController(List<Instruction> instructions) {
        activityLogger.setCaller(new ControllerCaller() {
            @Override
            public String getCallerType() {
                return "Instruction";
            }

            @Override
            public String getCallerOwner() {
                return "AutomatedController";
            }
        });
        this.instructions = instructions;
    }

    public AutomatedController(List<Instruction> instructions, Subscriber next) {
        activityLogger.setCaller(new ControllerCaller() {
            @Override
            public String getCallerType() {
                return "Instruction";
            }

            @Override
            public String getCallerOwner() {
                return "AutomatedController";
            }
        });
        this.instructions = instructions;
        this.next = next;
    }

    public void addInstruction(Instruction instruction){
        instructions.add(instruction);
    }

    public void removeInstruction(Instruction instruction){
        instructions.remove(instruction);
    }

    public void updateInstruction(Instruction instruction){
        int idInstruction = instruction.getIdInstruction();
        for (Instruction ownedInstruction:
             instructions) {
            if(ownedInstruction.getIdInstruction() == idInstruction){
                instructions.remove(ownedInstruction);
                instructions.add(instruction);
                return;
            }
        }

        addInstruction(instruction);
    }

    public void setActivityLoggerFile(String fileName){
        activityLogger.setLogFileName(fileName);
    }

    public List<Instruction> getInstructions() {
        return instructions;
    }

    public void setInstructions(List<Instruction> instructions) {
        this.instructions = instructions;
    }

    public ActivityLogger getActivityLogger() {
        return activityLogger;
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Subscriber getNext() {
        return next;
    }

    @Override
    public void setNext(Subscriber subscriber) {
        next = subscriber;
    }

    @Override
    public void update(SensorData data) {
        activityLogger.setCaller(new ControllerCaller() {
            @Override
            public String getCallerType() {
                return "Instruction";
            }

            @Override
            public String getCallerOwner() {
                return "AutomatedController";
            }
        });
        for (Instruction instruction:
             instructions) {
            if(instruction.shouldBeExecuted()){
                instruction.execute(activityLogger);
            }
        }

        if(next != null){
            next.update(data);
        }
    }

    public void callInstruction(ControllerCaller caller, int idInstruction){
        activityLogger.setCaller(caller);
        for (Instruction instruction:
             instructions) {
            if(instruction.getIdInstruction() == idInstruction){
                instruction.execute(activityLogger);
                return;
            }
        }
    }
}
