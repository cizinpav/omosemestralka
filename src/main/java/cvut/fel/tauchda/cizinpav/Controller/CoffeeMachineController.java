package cvut.fel.tauchda.cizinpav.Controller;

import cvut.fel.tauchda.cizinpav.Device.CoffeeMachine;
import cvut.fel.tauchda.cizinpav.DeviceCommands.DeviceCommand;

import java.util.List;
import java.util.Map;

public class CoffeeMachineController extends ManualController{
    public CoffeeMachineController(CoffeeMachine coffeeMachine) {
        super(coffeeMachine);
    }

    public CoffeeMachineController(CoffeeMachine coffeeMachine, Map<String, DeviceCommand> commands) {
        super(coffeeMachine, commands);
    }
}
