package cvut.fel.tauchda.cizinpav.Controller;

import cvut.fel.tauchda.cizinpav.Device.Tv;
import cvut.fel.tauchda.cizinpav.DeviceCommands.DeviceCommand;

import java.util.List;
import java.util.Map;

public class TvController extends ManualController{
    public TvController(Tv tv) {
        super(tv);
    }

    public TvController(Tv tv, Map<String, DeviceCommand> commands) {
        super(tv, commands);
    }
}
