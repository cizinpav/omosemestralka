package cvut.fel.tauchda.cizinpav.Controller;

import cvut.fel.tauchda.cizinpav.Device.Fridge;


/**
 * Subclass of {@link ManualController} for {@link Fridge}.
 */
public class FridgeController extends ManualController {

    /**
     * Constructor
     *
     * @param fridge {@link Fridge} for {@link FridgeController} to be associated with.
     */
    public FridgeController(Fridge fridge) {
        super(fridge);
    }
}
