package cvut.fel.tauchda.cizinpav.Controller;

import java.io.Serializable;

/**
 * Used to identified caller to {@link cvut.fel.tauchda.cizinpav.Logger.ActivityLogger}.
 */
public interface ControllerCaller extends Serializable {
    /**
     * Returns caller type (ex. mobile, REST api, instruction).
     *
     * @return caller type.
     */
    String getCallerType();

    /**
     * Returns caller owner (ex. Dad, Mom, automatic controller).
     *
     * @return caller owner.
     */
    String getCallerOwner();
}
