package cvut.fel.tauchda.cizinpav.Controller;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import cvut.fel.tauchda.cizinpav.Device.Device;
import cvut.fel.tauchda.cizinpav.DeviceCommands.DeviceCommand;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Abstract class for all controllers mounted on {@link Device}.
 */
@JsonIgnoreProperties(value = {"device"})
public abstract class ManualController implements Serializable {
    /**
     * {@link Device} for {@link ManualController} to be associated with.
     */
    private final Device device;

    /**
     * List of {@link DeviceCommand} which {@link ManualController} can perform.
     * (pozn. {@link ManualController} can perform more operation on {@link Device} even without {@link DeviceCommand})
     */
    private Map<String, DeviceCommand> commands;

    /**
     * Constructor without commands
     *
     * @param device {@link Device} to be associated with {@link ManualController}.
     */
    public ManualController(Device device) {
        this.device = device;
        commands = new HashMap<String, DeviceCommand>();
    }


    /**
     * Constructor
     *
     * @param device   {@link Device} to be associated with {@link ManualController}.
     * @param commands List of available {@link DeviceCommand}.
     */
    public ManualController(Device device, Map<String, DeviceCommand> commands) {
        this.device = device;
        this.commands = commands;
    }


    /**
     * {@link Device} getter;
     *
     * @return {@link Device} associated with {@link ManualController}
     */
    public Device getDevice() {
        return device;
    }

    /**
     * {@link DeviceCommand} getter.
     *
     * @return {@link DeviceCommand} associated with {@link ManualController}
     */
    @JsonGetter(value = "commands")
    public Map<String, DeviceCommand> getCommands() {
        return commands;
    }

    /**
     * {@link DeviceCommand} setter.
     *
     * @param commands Map of {@link DeviceCommand} to be used.
     */
    public void setCommands(Map<String, DeviceCommand> commands) {
        this.commands = commands;
    }

    public void addCommand(DeviceCommand command){
        this.commands.put(command.getAction(), command);
    }

    /**
     * Adds {@link DeviceCommand} to map of available {@link DeviceCommand}.
     *
     * @param commandName Name of {@link DeviceCommand} to add to map of available {@link DeviceCommand}.
     * @param command {@link DeviceCommand} to add to map of available {@link DeviceCommand}.
     */
    public void addCommand(String commandName, DeviceCommand command) {
        this.commands.put(commandName, command);
    }
}
