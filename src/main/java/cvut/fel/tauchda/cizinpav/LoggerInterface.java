package cvut.fel.tauchda.cizinpav;

import java.io.Serializable;

/**
 * Interface for loggers
 */
public interface LoggerInterface extends Serializable {
    /**
     * Log data
     */
    void log();
}
