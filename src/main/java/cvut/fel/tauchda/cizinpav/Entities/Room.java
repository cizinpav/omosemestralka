package cvut.fel.tauchda.cizinpav.Entities;

import cvut.fel.tauchda.cizinpav.Device.Device;
import cvut.fel.tauchda.cizinpav.Sensor.Sensor;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Room implements Serializable {
    private final int idRoom;
    private String name;
    private List<Device> devices = new ArrayList<>();
    private List<Sensor> sensors = new ArrayList<>();

    public Room(int idRoom, String name) {
        this.idRoom = idRoom;
        this.name = name;
    }

    public Room(int idRoom, String name, List<Device> devices, List<Sensor> sensors) {
        this.idRoom = idRoom;
        this.name = name;
        this.devices = devices;
        this.sensors = sensors;
    }

    public int getIdRoom() {
        return idRoom;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Device> getDevices() {
        return devices;
    }

    public Device getDevice(int id){
        for (Device device:
                devices) {
            if(device.getIdDevice() == id){
                return device;
            }
        }

        System.err.println("Device with id:"+ String.valueOf(id) +" not found!");
        return null;
    }

    public void setDevices(List<Device> devices) {
        this.devices = devices;
    }

    public void addDevice(Device device){
        this.devices.add(device);
    }

    public void removeDevice(Device device){
        this.devices.add(device);
    }

    public List<Sensor> getSensors() {
        return sensors;
    }

    public void setSensors(List<Sensor> sensors) {
        this.sensors = sensors;
    }

    public void addSensor(Sensor sensor){
        this.sensors.add(sensor);
    }

    public void removeSensor(Sensor sensor){
        this.sensors.remove(sensor);
    }

    public Sensor getSensor(int idSensor) {
        for (Sensor sensor:
                sensors) {
            if(sensor.getIdSensor() == idSensor){
                return sensor;
            }
        }

        System.err.println("Device with id:"+ String.valueOf(idSensor) +" not found!");
        return null;
    }
}
