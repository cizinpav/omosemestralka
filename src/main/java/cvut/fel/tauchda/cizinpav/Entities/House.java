package cvut.fel.tauchda.cizinpav.Entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import cvut.fel.tauchda.cizinpav.Controller.AutomatedController;
import cvut.fel.tauchda.cizinpav.Sensor.SensorUpdateClock;

import java.io.Serializable;
import java.rmi.NoSuchObjectException;
import java.util.ArrayList;
import java.util.List;

public class House implements Serializable {
    private final int idHome;
    private List<Floor> floors = new ArrayList<>();
    private List<Occupant> occupants = new ArrayList<>();
    private List<Pet> pets = new ArrayList<>();
    private List<Transport> transports = new ArrayList<>();
    private AutomatedController controller = new AutomatedController();
    private String HomeType;
    private String Street;
    private String Town;
    @JsonIgnore
    private SensorUpdateClock clock;

    public House(int idHome,String HomeType,String Street,String Town){
        this.idHome=idHome;
        this.HomeType=HomeType;
        this.Street=Street;
        this.Town=Town;
    }

    public House(int idHome, List<Floor> floors, List<Occupant> occupants, List<Pet> pets, List<Transport> transports, AutomatedController controller, String homeType, String street, String town) {
        this.idHome = idHome;
        this.floors = floors;
        this.occupants = occupants;
        this.pets = pets;
        this.transports = transports;
        this.controller = controller;
        HomeType = homeType;
        Street = street;
        Town = town;
    }

    public SensorUpdateClock getClock() {
        return clock;
    }

    public void setClock(SensorUpdateClock clock) {
        this.clock = clock;
    }

    public int getIdHome() {
        return idHome;
    }

    public List<Floor> getFloors() {
        return floors;
    }

    public Floor getFloor(int id){
        for (Floor floor:
             floors) {
            if(floor.getIdFloor() == id){
                return floor;
            }
        }

        System.err.println("Floor with id:"+ String.valueOf(id) +" not found!");
        return null;
    }

    public void setFloors(List<Floor> floors) {
        this.floors = floors;
    }

    public void addFloor(Floor floor){
        this.floors.add(floor);
    }

    public void removeFloor(Floor floor){
        this.floors.remove(floor);
    }

    public List<Occupant> getOccupants() {
        return occupants;
    }

    public Occupant getOccupant(int id){
        for (Occupant occupant:
                occupants) {
            if(occupant.getIdOccupant() == id){
                return occupant;
            }
        }

        System.err.println("Occupant with id:"+ String.valueOf(id) +" not found!");
        return null;
    }

    public void setOccupants(List<Occupant> occupants) {
        this.occupants = occupants;
    }

    public void addOccupant(Occupant occupant) {
        this.occupants.add(occupant);
    }

    public void removeOccupant(Occupant occupant){
        this.occupants.remove(occupant);
    }

    public List<Pet> getPets() {
        return pets;
    }

    public Pet getPet(int id){
        for (Pet pet:
                pets) {
            if(pet.getIdPet() == id){
                return pet;
            }
        }

        System.err.println("Pet with id:"+ String.valueOf(id) +" not found!");
        return null;
    }

    public void setPets(List<Pet> pets) {
        this.pets = pets;
    }

    public void addPet(Pet pet){
        this.pets.add(pet);
    }

    public void removePet(Pet pet){
        this.pets.remove(pet);
    }

    public List<Transport> getTransports() {
        return transports;
    }

    public Transport getTransport(int id){
        for (Transport transport:
                transports) {
            if(transport.getIdTransport() == id){
                return transport;
            }
        }

        System.err.println("Transport with id:"+ String.valueOf(id) +" not found!");
        return null;
    }

    public void setTransports(List<Transport> transports) {
        this.transports = transports;
    }

    public void addTransport(Transport transport){
        this.transports.add(transport);
    }

    public void removeTransport(Transport transport){
        this.transports.remove(transport);
    }

    public AutomatedController getController() {
        return controller;
    }

    public void setController(AutomatedController controller) {
        this.controller = controller;
    }

    public String getHomeType() {
        return HomeType;
    }

    public void setHomeType(String homeType) {
        HomeType = homeType;
    }

    public String getStreet() {
        return Street;
    }

    public void setStreet(String street) {
        Street = street;
    }

    public String getTown() {
        return Town;
    }

    public void setTown(String town) {
        Town = town;
    }

}
