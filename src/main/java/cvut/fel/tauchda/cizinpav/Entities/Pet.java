package cvut.fel.tauchda.cizinpav.Entities;

import java.io.Serializable;

public class Pet implements Serializable {
    private final int idPet;
    private final String name;

    public Pet(int idPet, String name) {
        this.idPet = idPet;
        this.name = name;
    }

    public int getIdPet() {
        return idPet;
    }

    public String getName() {
        return name;
    }
}
