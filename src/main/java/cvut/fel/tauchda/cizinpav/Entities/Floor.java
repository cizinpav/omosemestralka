package cvut.fel.tauchda.cizinpav.Entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Floor implements Serializable {
    public final int idFloor;
    public List<Room> rooms = new ArrayList<>();

    public Floor(int idFloor){
        this.idFloor =idFloor;
    }

    public int getIdFloor() {
        return idFloor;
    }

    public List<Room> getRooms() {
        return rooms;
    }

    public Room getRoom(int id){
        for (Room room:
             rooms) {
            if(room.getIdRoom() == id){
                return room;
            }
        }

        System.err.println("Room with id:"+ String.valueOf(id) +" not found!");
        return null;
    }

    public void setRooms(List<Room> rooms) {
        this.rooms = rooms;
    }

    public void addRoom(Room room){
        this.rooms.add(room);
    }

    public void removeRoom(Room room){
        this.rooms.remove(room);
    }
}
