package cvut.fel.tauchda.cizinpav.Entities;

public class Car extends Transport {

    private String CarPrice;

    private String CarBrand;

    private String CarType;
    public Car(int idCar,String CarPrice,String CarBrand,String CarType){
        super(idCar);
        this.CarPrice=CarPrice;
        this.CarBrand=CarBrand;
        this.CarType=CarType;
    }

    public String getCarPrice() {
        return CarPrice;
    }

    public void setCarPrice(String carPrice) {
        CarPrice = carPrice;
    }
    public String getCarBrand() {
        return CarBrand;
    }

    public void setCarBrand(String carBrand) {
        CarBrand = carBrand;
    }
    public String getCarType() {
        return CarType;
    }

    public void setCarType(String carType) {
        CarType = carType;
    }
}
