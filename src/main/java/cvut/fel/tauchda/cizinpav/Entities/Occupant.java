package cvut.fel.tauchda.cizinpav.Entities;

import java.io.Serializable;

public class Occupant implements Serializable {
    private final int idOccupant;
    private final String name;


    public Occupant(int idOccupant, String name) {
        this.idOccupant = idOccupant;
        this.name = name;
    }

    public int getIdOccupant() {
        return idOccupant;
    }

    public String getName() {
        return name;
    }
}
