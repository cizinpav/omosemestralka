package cvut.fel.tauchda.cizinpav.Entities;

import cvut.fel.tauchda.cizinpav.Controller.AutomatedController;
import cvut.fel.tauchda.cizinpav.Controller.ControllerCaller;

import java.io.Serializable;

public class Mobile implements Serializable {
    private final Occupant owner;
    private final ControllerCaller caller = new ControllerCaller() {
        @Override
        public String getCallerType() {
            return "Mobile";
        }

        @Override
        public String getCallerOwner() {
            return owner.getName();
        }
    };

    public Mobile(Occupant owner) {
        this.owner = owner;
    }

    public void callInstruction(AutomatedController controller, int idInstruction){
        controller.callInstruction(caller, idInstruction);
    }

    public Occupant getOwner() {
        return owner;
    }

    public ControllerCaller getCaller() {
        return caller;
    }
}
