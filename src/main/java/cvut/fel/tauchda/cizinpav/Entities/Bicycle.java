package cvut.fel.tauchda.cizinpav.Entities;

public class Bicycle extends Transport {
    private String type;

    private String brand;

    public Bicycle(int idBicycle,String Type,String Brand) {
        super(idBicycle);
        this.type = Type;
        this.brand = Brand;

    }
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }
}
