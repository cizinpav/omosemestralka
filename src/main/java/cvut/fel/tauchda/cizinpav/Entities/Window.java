package cvut.fel.tauchda.cizinpav.Entities;

public class Window {
    private int idWindow;
    private String type;
    public Window(int idWindow, String type) {
        this.idWindow=idWindow;
        this.type=type;
    }
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
    public int getIdWindow() {
        return idWindow;
    }

    public void setIdWindow(int idWindow) {
        this.idWindow = idWindow;
    }


}
