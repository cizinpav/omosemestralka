package cvut.fel.tauchda.cizinpav.Entities;

import java.io.Serializable;

public abstract class Transport implements Serializable {
    private final int idTransport;
    private double mileage = 0;

    public Transport(int idTransport) {
        this.idTransport = idTransport;
    }

    public Transport(int idTransport, double mileage) {
        this.idTransport = idTransport;
        this.mileage = mileage;
    }

    public int getIdTransport() {
        return idTransport;
    }

    public double getMileage() {
        return mileage;
    }

    public void setMileage(double mileage) {
        this.mileage = mileage;
    }

    public void addMileage(double addMileage){
        this.mileage += addMileage;
    }

    public void travel(double distance){
        addMileage(distance);
    }
}
