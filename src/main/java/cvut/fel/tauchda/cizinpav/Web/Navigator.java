package cvut.fel.tauchda.cizinpav.Web;

import cvut.fel.tauchda.cizinpav.Controller.AutomatedController;
import cvut.fel.tauchda.cizinpav.Controller.ManualController;
import cvut.fel.tauchda.cizinpav.Device.Device;
import cvut.fel.tauchda.cizinpav.Entities.*;
import cvut.fel.tauchda.cizinpav.Sensor.Sensor;

import java.util.List;

public class Navigator {
    private List<House> houses;

    public Navigator(List<House> houses) {
        this.houses = houses;
    }

    public List<House> getHouses(){
        return houses;
    }

    public House getHouse(int idHouse){
        for (House house:
                houses) {
            if(house.getIdHome() == idHouse){
                return house;
            }
        }
        System.err.println("House with id:"+String.valueOf(idHouse)+" not found!");

        return null;
    }

    public List<Floor> getFloors(int idHouse){
        for (House house:
                houses) {
            if(house.getIdHome() == idHouse){
                return house.getFloors();
            }
        }
        System.err.println("House with id:"+String.valueOf(idHouse)+" not found!");

        return null;
    }

    public Floor getFloor(int idHouse, int idFloor){
        for (House house:
                houses) {
            if(house.getIdHome() == idHouse){
                return house.getFloor(idFloor);
            }
        }
        System.err.println("House with id:"+String.valueOf(idHouse)+" not found!");

        return null;
    }

    public AutomatedController getAutomatedController(int idHouse){
        for (House house:
                houses) {
            if(house.getIdHome() == idHouse){
                return house.getController();
            }
        }
        System.err.println("House with id:"+String.valueOf(idHouse)+" not found!");

        return null;
    }

    public List<Room> getRooms(int idHouse, int idFloor){
        for (House house:
        houses) {
            if(house.getIdHome() == idHouse){
                return house.getFloor(idFloor).getRooms();
            }
        }
        System.err.println("House with id:"+String.valueOf(idHouse)+" not found!");

        return null;
    }

    public Room getRoom(int idHouse, int idFloor, int idRoom){
        for (House house:
        houses) {
            if(house.getIdHome() == idHouse){
                return house.getFloor(idFloor).getRoom(idRoom);
            }
        }
        System.err.println("House with id:"+String.valueOf(idHouse)+" not found!");

        return null;
    }

    public List<Device> getDevices(int idHouse, int idFloor, int idRoom){
        for (House house:
                houses) {
            if(house.getIdHome() == idHouse){
                return house.getFloor(idFloor).getRoom(idRoom).getDevices();
            }
        }
        System.err.println("House with id:"+String.valueOf(idHouse)+" not found!");

        return null;
    }

    public Device getDevice(int idHouse, int idFloor, int idRoom, int idDevice){
        for (House house:
                houses) {
            if(house.getIdHome() == idHouse){
                return house.getFloor(idFloor).getRoom(idRoom).getDevice(idDevice);
            }
        }
        System.err.println("House with id:"+String.valueOf(idHouse)+" not found!");

        return null;
    }

    public ManualController getController(int idHouse, int idFloor, int idRoom, int idDevice){
        for (House house:
                houses) {
            if(house.getIdHome() == idHouse){
                return house.getFloor(idFloor).getRoom(idRoom).getDevice(idDevice).getController();
            }
        }
        System.err.println("House with id:"+String.valueOf(idHouse)+" not found!");

        return null;
    }

    public List<Occupant> getOccupants(int idHouse){
        for (House house:
                houses) {
            if(house.getIdHome() == idHouse){
                return house.getOccupants();
            }
        }
        System.err.println("House with id:"+String.valueOf(idHouse)+" not found!");

        return null;
    }

    public Occupant getOccupant(int idHouse, int idOccupant){
        for (House house:
                houses) {
            if(house.getIdHome() == idHouse){
                return house.getOccupant(idOccupant);
            }
        }
        System.err.println("House with id:"+String.valueOf(idHouse)+" not found!");

        return null;
    }

    public List<Pet> getPets(int idHouse){
        for (House house:
                houses) {
            if(house.getIdHome() == idHouse){
                return house.getPets();
            }
        }
        System.err.println("House with id:"+String.valueOf(idHouse)+" not found!");

        return null;
    }

    public Pet getPet(int idHouse, int idPet){
        for (House house:
                houses) {
            if(house.getIdHome() == idHouse){
                return house.getPet(idPet);
            }
        }
        System.err.println("House with id:"+String.valueOf(idHouse)+" not found!");

        return null;
    }

    public List<Transport> getTransports(int idHouse){
        for (House house:
                houses) {
            if(house.getIdHome() == idHouse){
                return house.getTransports();
            }
        }
        System.err.println("House with id:"+String.valueOf(idHouse)+" not found!");

        return null;
    }

    public Transport getTransport(int idHouse, int idTransport){
        for (House house:
                houses) {
            if(house.getIdHome() == idHouse){
                return house.getTransport(idTransport);
            }
        }
        System.err.println("House with id:"+String.valueOf(idHouse)+" not found!");

        return null;
    }

    public List<Sensor> getSensors(int idHouse, int idFloor, int idRoom){
        for (House house:
                houses) {
            if(house.getIdHome() == idHouse){
                return house.getFloor(idFloor).getRoom(idRoom).getSensors();
            }
        }
        System.err.println("House with id:"+String.valueOf(idHouse)+" not found!");

        return null;
    }

    public Sensor getSensor(int idHouse, int idFloor, int idRoom, int idSensor){
        for (House house:
                houses) {
            if(house.getIdHome() == idHouse){
                return house.getFloor(idFloor).getRoom(idRoom).getSensor(idSensor);
            }
        }
        System.err.println("House with id:"+String.valueOf(idHouse)+" not found!");

        return null;
    }
}
