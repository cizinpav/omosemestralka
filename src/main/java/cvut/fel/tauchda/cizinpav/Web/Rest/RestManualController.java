package cvut.fel.tauchda.cizinpav.Web.Rest;

import cvut.fel.tauchda.cizinpav.Controller.ManualController;
import cvut.fel.tauchda.cizinpav.Device.Fridge;
import cvut.fel.tauchda.cizinpav.Sensor.FridgeSensor;
import cvut.fel.tauchda.cizinpav.Web.WebStarter;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/house/{idHouse}/floor/{idFloor}/room/{idRoom}/device/{idDevice}/controller")
/**
 *Class to manage ManualControler
 */
public class RestManualController {
    /**
     * method to get manualcontroler
     */
    @GetMapping(value = {"", "/"}, produces = "application/json")
    public ManualController getDevices(@PathVariable("idHouse") int idHouse, @PathVariable("idFloor") int idFloor, @PathVariable("idRoom") int idRoom, @PathVariable("idDevice") int idDevice){
        return WebStarter.navigator.getController(idHouse, idFloor, idRoom, idDevice);
    }
}
