package cvut.fel.tauchda.cizinpav.Web;

import cvut.fel.tauchda.cizinpav.Configuration.Configurator;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebStarter {
    public static Navigator navigator;

    public static void main(String[] args) {
        System.out.println("Starting system");
        // Loads configuration
        // Default path is ./configuration.ser
        // To read another configuration pass path as string to constructor (without file extension)
        System.out.println("Setting up configurator");
        Configurator configurator = new Configurator();

        // Loads configuration
        System.out.println("Loading configuration");
        navigator = new Navigator(configurator.loadConfiguration());
        // Run houses clock to generate some random data on sensors
        // runWait(int iterations, int sleepSeconds) will update all sensors x times (x = iterations in passed arguments) and wait sleepSeconds second between iterations
        System.out.println("Flooding cache with data");
        navigator.getHouse(0).getClock().run(10);


        // After some data was generated start webserver for api
        System.out.println("Starting web server");
        SpringApplication.run(WebStarter.class, args);
    }
}
