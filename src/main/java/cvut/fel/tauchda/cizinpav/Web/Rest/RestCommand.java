package cvut.fel.tauchda.cizinpav.Web.Rest;

import cvut.fel.tauchda.cizinpav.Controller.ControllerCaller;
import cvut.fel.tauchda.cizinpav.Controller.ManualController;
import cvut.fel.tauchda.cizinpav.Logger.UserActivityLogger;
import cvut.fel.tauchda.cizinpav.Web.WebStarter;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/command")
public class RestCommand {

    @PostMapping("/{commandName}/house/{houseId}/floor/{floorId}/room/{roomId}/device/{deviceId}")
    public void callCommand(@PathVariable String commandName, @PathVariable int deviceId, @PathVariable int floorId, @PathVariable int houseId, @PathVariable int roomId){
        ManualController controller = WebStarter.navigator.getController(houseId, floorId, roomId, deviceId);
        UserActivityLogger logger = UserActivityLogger.getInstance();
        logger.setCaller(new ControllerCaller() {
            @Override
            public String getCallerType() {
                return "Online request";
            }

            @Override
            public String getCallerOwner() {
                return "Rest api";
            }
        });
        logger.setDevice(WebStarter.navigator.getDevice(houseId, floorId, roomId, deviceId));
        logger.setAction(commandName);

        controller.getCommands().get(commandName).execute();

        logger.log();
    }
}
