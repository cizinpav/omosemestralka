package cvut.fel.tauchda.cizinpav.Web.Rest;

import cvut.fel.tauchda.cizinpav.Device.Fridge;
import cvut.fel.tauchda.cizinpav.Entities.Pet;
import cvut.fel.tauchda.cizinpav.Sensor.FridgeSensor;
import cvut.fel.tauchda.cizinpav.Web.WebStarter;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/house/{idHouse}/pet")
/**
 * Class to manage Pet
 */
public class RestPet {
    /**
     * Method to get pet
     */
    @GetMapping(value = {"", "/"}, produces = "application/json")
    public List<Pet> getPets(@PathVariable("idHouse") int idHouse){
        return WebStarter.navigator.getPets(idHouse);
    }

    @GetMapping(value = "/{idPet}", produces = "application/json")
    public Pet getPet(@PathVariable("idHouse") int idHouse, @PathVariable("idPet") int idPet){
        return WebStarter.navigator.getPet(idHouse, idPet);
    }
}
