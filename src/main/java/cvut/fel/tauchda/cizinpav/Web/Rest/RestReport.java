package cvut.fel.tauchda.cizinpav.Web.Rest;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import cvut.fel.tauchda.cizinpav.Device.Fridge;
import cvut.fel.tauchda.cizinpav.Sensor.FridgeSensor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.io.IOException;

@RestController
@RequestMapping("/report")
/**
 * Class to control report
 */
public class RestReport {
    /**
     * methods to get reports
     */

    @GetMapping(value = "/activity", produces = "application/json")
    public JsonNode getActivityReport(){
        ObjectMapper json = new ObjectMapper();
        try {
            return json.readValue(new File("activityReport.json"), JsonNode.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @GetMapping(value = "/consumption", produces = "application/json")
    public JsonNode getConsumptionReport(){
        ObjectMapper json = new ObjectMapper();
        try {
            return json.readValue(new File("consumptionReport.json"), JsonNode.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
