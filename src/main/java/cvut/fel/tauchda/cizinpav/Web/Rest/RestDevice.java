package cvut.fel.tauchda.cizinpav.Web.Rest;

import cvut.fel.tauchda.cizinpav.Device.Device;
import cvut.fel.tauchda.cizinpav.Device.Fridge;
import cvut.fel.tauchda.cizinpav.Sensor.FridgeSensor;
import cvut.fel.tauchda.cizinpav.Web.WebStarter;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/house/{idHouse}/floor/{idFloor}/room/{idRoom}/device")
/**
 * Class to manage Device
 */
public class RestDevice {
    /**
     * Methods to get Device
     * @param idHouse
     * @param idRoom
     * @param idFloor
     */
    @GetMapping(value = {"", "/"}, produces = "application/json")
    public List<Device> getDevices(@PathVariable("idHouse") int idHouse, @PathVariable("idFloor") int idFloor, @PathVariable("idRoom") int idRoom){
        return WebStarter.navigator.getDevices(idHouse, idFloor, idRoom);
    }

    @GetMapping(value = {"/{idDevice}"}, produces = "application/json")
    public Device getDevice(@PathVariable("idHouse") int idHouse, @PathVariable("idFloor") int idFloor, @PathVariable("idRoom") int idRoom, @PathVariable("idDevice") int idDevice){
        return WebStarter.navigator.getDevice(idHouse, idFloor, idRoom, idDevice);
    }
}
