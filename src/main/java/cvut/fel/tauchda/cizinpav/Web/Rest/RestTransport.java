package cvut.fel.tauchda.cizinpav.Web.Rest;

import cvut.fel.tauchda.cizinpav.Device.Fridge;
import cvut.fel.tauchda.cizinpav.Entities.Occupant;
import cvut.fel.tauchda.cizinpav.Entities.Transport;
import cvut.fel.tauchda.cizinpav.Sensor.FridgeSensor;
import cvut.fel.tauchda.cizinpav.Web.WebStarter;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/house/{idHouse}/transport")
/**
 * class to manage transport
 */
public class RestTransport {
    /**
     * methods to get transport
     */
    @GetMapping(value = {"", "/"}, produces = "application/json")
    public List<Transport> getTransports(@PathVariable("idHouse") int idHouse){
        return WebStarter.navigator.getTransports(idHouse);
    }

    @GetMapping(value = "/{idTransport}", produces = "application/json")
    public Transport getTransport(@PathVariable("idHouse") int idHouse, @PathVariable("idTransport") int idTransport){
        return WebStarter.navigator.getTransport(idHouse, idTransport);
    }
}
