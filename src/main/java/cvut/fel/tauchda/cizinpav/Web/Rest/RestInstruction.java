package cvut.fel.tauchda.cizinpav.Web.Rest;

import cvut.fel.tauchda.cizinpav.Controller.AutomatedController;
import cvut.fel.tauchda.cizinpav.Controller.ControllerCaller;
import cvut.fel.tauchda.cizinpav.Controller.ManualController;
import cvut.fel.tauchda.cizinpav.Logger.UserActivityLogger;
import cvut.fel.tauchda.cizinpav.Web.WebStarter;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/instruction")
public class RestInstruction {

    @PostMapping("/{instructionId}/house/{houseId}")
    public void callInstruction(@PathVariable int instructionId, @PathVariable int houseId){
        AutomatedController controller = WebStarter.navigator.getAutomatedController(houseId);
        controller.callInstruction(new ControllerCaller() {
            @Override
            public String getCallerType() {
                return "Online request";
            }

            @Override
            public String getCallerOwner() {
                return "Rest api";
            }
        }, instructionId);
    }
}
