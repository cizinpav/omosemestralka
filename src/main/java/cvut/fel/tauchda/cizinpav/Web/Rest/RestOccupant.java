package cvut.fel.tauchda.cizinpav.Web.Rest;

import cvut.fel.tauchda.cizinpav.Device.Fridge;
import cvut.fel.tauchda.cizinpav.Entities.Occupant;
import cvut.fel.tauchda.cizinpav.Sensor.FridgeSensor;
import cvut.fel.tauchda.cizinpav.Web.WebStarter;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/house/{idHouse}/occupant")
/**
 * class to manageOccupant
 */
public class RestOccupant {
    /**
     * method to get occupant
     */
    @GetMapping(value = {"", "/"}, produces = "application/json")
    public List<Occupant> getOccupants(@PathVariable("idHouse") int idHouse){
        return WebStarter.navigator.getOccupants(idHouse);
    }

    @GetMapping(value = "/{idOccupant}", produces = "application/json")
    public Occupant getOccupant(@PathVariable("idHouse") int idHouse, @PathVariable("idOccupant") int idOccupant){
        return WebStarter.navigator.getOccupant(idHouse, idOccupant);
    }
}
