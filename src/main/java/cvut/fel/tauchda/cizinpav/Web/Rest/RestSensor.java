package cvut.fel.tauchda.cizinpav.Web.Rest;

import cvut.fel.tauchda.cizinpav.Device.Device;
import cvut.fel.tauchda.cizinpav.Device.Fridge;
import cvut.fel.tauchda.cizinpav.Sensor.FridgeSensor;
import cvut.fel.tauchda.cizinpav.Sensor.Sensor;
import cvut.fel.tauchda.cizinpav.Web.WebStarter;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/house/{idHouse}/floor/{idFloor}/room/{idRoom}/sensor")
/**
 * Class to manage senzor
 */
public class RestSensor {
    /**
     * method to get sensors
     */
    @GetMapping(value = {"", "/"}, produces = "application/json")
    public List<Sensor> getSensors(@PathVariable("idHouse") int idHouse, @PathVariable("idFloor") int idFloor, @PathVariable("idRoom") int idRoom){
        return WebStarter.navigator.getSensors(idHouse, idFloor, idRoom);
    }

    @GetMapping(value = {"/{idSensor}"}, produces = "application/json")
    public Sensor getSensor(@PathVariable("idHouse") int idHouse, @PathVariable("idFloor") int idFloor, @PathVariable("idRoom") int idRoom, @PathVariable("idSensor") int idSensor){
        return WebStarter.navigator.getSensor(idHouse, idFloor, idRoom, idSensor);
    }
}
