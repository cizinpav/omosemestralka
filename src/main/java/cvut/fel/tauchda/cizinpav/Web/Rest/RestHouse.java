package cvut.fel.tauchda.cizinpav.Web.Rest;

import cvut.fel.tauchda.cizinpav.Controller.AutomatedController;
import cvut.fel.tauchda.cizinpav.Device.Fridge;
import cvut.fel.tauchda.cizinpav.Entities.House;
import cvut.fel.tauchda.cizinpav.Entities.Occupant;
import cvut.fel.tauchda.cizinpav.Entities.Pet;
import cvut.fel.tauchda.cizinpav.Sensor.FridgeSensor;
import cvut.fel.tauchda.cizinpav.Web.WebStarter;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/house")
/**
 * Class to manage House
 */
public class RestHouse {
    /**
     * method to get House
     */
    @GetMapping(value = {"", "/"}, produces = "application/json")
    public List<House> getHouses(){
        return WebStarter.navigator.getHouses();
    }

    @GetMapping(value = "/{idHouse}", produces = "application/json")
    public House getHouse(@PathVariable("idHouse") int idHouse){
        return WebStarter.navigator.getHouse(idHouse);
    }


    @GetMapping(value = {"/{idHouse}/automatedController", "/{idHouse}/automatedController/"}, produces = "application/json")
    public AutomatedController getAutomatedController(@PathVariable("idHouse") int idHouse){
        return WebStarter.navigator.getAutomatedController(idHouse);
    }
}
