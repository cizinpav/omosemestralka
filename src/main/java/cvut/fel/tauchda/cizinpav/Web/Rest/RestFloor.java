package cvut.fel.tauchda.cizinpav.Web.Rest;

import cvut.fel.tauchda.cizinpav.Controller.AutomatedController;
import cvut.fel.tauchda.cizinpav.Device.Fridge;
import cvut.fel.tauchda.cizinpav.Entities.Floor;
import cvut.fel.tauchda.cizinpav.Entities.Room;
import cvut.fel.tauchda.cizinpav.Sensor.FridgeSensor;
import cvut.fel.tauchda.cizinpav.Web.WebStarter;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/house/{idHouse}/")
/**
 * Class to manage Floor
 */
public class RestFloor {
    /**
     * methods to get Floor
     * @param idHouse
     */
    @GetMapping(value = {"", "/"}, produces = "application/json")
    public List<Floor> getFloors(@PathVariable("idHouse") int idHouse){
        return WebStarter.navigator.getFloors(idHouse);
    }

    @GetMapping(value = {"/floor/{idFloor}"}, produces = "application/json")
    public Floor getFloor(@PathVariable("idHouse") int idHouse, @PathVariable("idFloor") int idFloor){
        return WebStarter.navigator.getFloor(idHouse, idFloor);
    }
}
