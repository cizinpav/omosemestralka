package cvut.fel.tauchda.cizinpav.Web.Rest;

import cvut.fel.tauchda.cizinpav.Controller.AutomatedController;
import cvut.fel.tauchda.cizinpav.Device.Fridge;
import cvut.fel.tauchda.cizinpav.Entities.Floor;
import cvut.fel.tauchda.cizinpav.Entities.Room;
import cvut.fel.tauchda.cizinpav.Sensor.FridgeSensor;
import cvut.fel.tauchda.cizinpav.Web.WebStarter;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/house/{idHouse}/floor/{idFloor}/room")
/**
 * Class to manage Room
 */
public class RestRoom {
    /**
     * method to get room
     */

    @GetMapping(value = {"", "/"}, produces = "application/json")
    public List<Room> getRooms(@PathVariable("idHouse") int idHouse, @PathVariable("idFloor") int idFloor){
        return WebStarter.navigator.getRooms(idHouse, idFloor);
    }

    @GetMapping(value = {"/{idRoom}"}, produces = "application/json")
    public Room getRoom(@PathVariable("idHouse") int idHouse, @PathVariable("idFloor") int idFloor, @PathVariable("idRoom") int idRoom){
        return WebStarter.navigator.getRoom(idHouse, idFloor, idRoom);
    }
}

