package cvut.fel.tauchda.cizinpav.Configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import cvut.fel.tauchda.cizinpav.Entities.House;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Configurator {
    private String fileName = "configuration";
    private final String fileExtension = ".ser";

    public Configurator() {
    }

    public Configurator(String fileName) {
        this.fileName = fileName;
    }

    public List<House> loadConfiguration(){
        try {
            FileInputStream fileIn = new FileInputStream(fileName+fileExtension);
            ObjectInputStream objectIn = new ObjectInputStream(fileIn);

            return (List<House>) objectIn.readObject();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void saveConfiguration(List<House> houses){
        try {
            FileOutputStream fileOut = new FileOutputStream(fileName+fileExtension);
            ObjectOutputStream objectOut = new ObjectOutputStream(fileOut);

            objectOut.writeObject(houses);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileExtension() {
        return fileExtension;
    }
}
